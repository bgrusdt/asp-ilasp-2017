"""ILASP alternate implementation

Usage:
  ilasp.py solve <problem_def_file> [--algorithm=<algo>] [--logdir=<dir>] [--searchspace=<file>]
  ilasp.py build_searchspace <problem_def_file> [<up_to_length>] [--logdir=<dir>]
  ilasp.py info [<topic>]
  ilasp.py (-h | --help)
  ilasp.py --version

Options:
  -h --help             Show this message.
  --version             Show version number.
  --algorithm=<algo>    Which algorithm to use for solving [default: iter_neg_pos].
  --logdir=<dir>        Specify the output dir for the logs/profiling data [default: logs].
  --searchspace=<file>  Specify a pre-generated searchspace.

"""
from __future__ import print_function
import json
import os.path
import constants
import sys

import searchspace
import searchspace.pre_generated
from algorithm.skeleton import LOGGER
from constants import RULE_PREFIX, VAR_PREFIX
from data import ProblemInstance
from utils.logger import IlaspLogger
from utils.profile import profile_section, TimeEncoder
from utils.generators import IDGenerator
from ilasp_control import rule_as_str


# FIXME turn into wrapper e.g. LazyConfig
def process_config(config):
    try:
        extra_args = {}
        # FIXME make it a default section or so
        for (key, val) in config.items('Meta'):
            if val.isdigit():
                val = int(val)

            extra_args[key] = val
    except RuntimeError as _:
        LOGGER.error("property file does not have meta section")
        extra_args = {}


    max_hypothesis_cost = extra_args.get('max_hypothesis_cost', constants.DEFAULT_MAX_HYPOTHESIS_COST)
    extra_args['max_hypothesis_cost'] = max_hypothesis_cost
    extra_args['max_rule_length'] = extra_args.get('max_rule_length', max_hypothesis_cost)

    return extra_args


def info(arguments):
    topic = arguments['<topic>']
    if topic is None:
        print("\n".join([
            "Possible topics are:",
            "- algorithm",
            "- problem_def_file",
            "- searchspace"
        ]))
    else:
        topic = topic.strip('-')
        if topic == "algorithm":
            print("\n".join([
                "Possible Algorithm are:",
                "- iter_neg_pos:",
                "    The default algorithm as described in the ILASP paper.",
                "    It is a iterative algorithm which first runs a negative",
                "    phase in which it finds \"illegal\" solutions and adds constraints",
                "    to prevent them and then runs a positive phase to find actual",
                "    valid solutions.",
                "- iter_pos_validate:",
                "    A iterative algorithm which in each iteration first searches for",
                "    any solution which entails all positive examples and then validates",
                "    all found solutions filtering out the solutions which entail any",
                "    negative example.",
            ]))
        elif topic == "problem_def_file":
            print("\n".join([
                "A path to a file containing the problem descriptions. Wrt. to the",
                "contained examples the file for each example is the file with the",
                "name 'ilasp.properties' in the example dictionary",
            ]))
        elif topic == "searchspace":
            print("\n".join([
                "Using --searchspace can specify a pre-generated searchspace.",
                "If a pre-generated searchspace is used no rules will be genrated by",
                "this algorithm at all, only the predefined rules will be used.",
                "",
                "The expected file format uses the syntax \"<length> ~ <asp_rule>\".",
                "Empty lines and such with a trainling \'#\' will be ignored.",
                "The parser does _not_ validate if a given asp_rule actually has the",
                "cost it was specified for.",
                "",
                "The ILASP Algorithm will still be executed iterative only including rules",
                "with a length <= current hypothesis cost (and <= the max_rule_length parameter)"
            ]))
        else:
            print("\n".join([
                "Unknown topic. Possible topics are:",
                "- algorithm",
                "- problem_def_file",
            ]))
            sys.exit(1)
    sys.exit(0)


def write_profiling_data(profiler_top, logdir):
    times = profiler_top.get_times()
    path = os.path.join(logdir, 'profiling.json')
    with open(path, 'wb') as fp:
        json.dump(times, fp, cls=TimeEncoder, indent=2)


def setup_logdir(arguments):
    logdir = arguments['--logdir']
    try:
        os.makedirs(logdir)
    except OSError as e:
        # we are ok if the path already exists,
        # through there should be is a more proper way to classify the error
        if "exists" not in str(e):
            raise e

    level = os.environ.get('LOG_LEVEL', 'INFO')
    IlaspLogger.global_setup(logdir, level)
    return logdir


def create_ilasp_instance(arguments, *args, **kwargs):
    algorithm = arguments['--algorithm']
    if algorithm == 'iter_neg_pos':
        import algorithm.iter_neg_pos as impl
    elif algorithm == 'iter_pos_validate':
        import algorithm.iter_pos_validate as impl
    else:
        print("unknown argument value for --algorithm:", algorithm, file=sys.stderr)
        sys.exit(2)

    return impl.ILASP(*args, **kwargs)


def create_search_space(arguments, problem, extra_args):
    searchspace_file = arguments['--searchspace']
    if not searchspace_file:
        return searchspace.SearchSpace(
            ruleId_generator=IDGenerator(RULE_PREFIX),
            var_id_generator=IDGenerator(VAR_PREFIX),
            path_to_mode_declarations=problem.mode_declaration_path,
            extra_args=extra_args,
            type_manager=problem.type_manager
        )
    else:
        return searchspace.pre_generated.load(arguments['--searchspace'], extra_args)


def run_solve(ilasp, max_hypothesis_cost):
    solution_sets = ilasp.run_ilasp(max_hypothesis_cost=max_hypothesis_cost)

    for idx, solution in enumerate(solution_sets):
        print("----------------------------------------------------------")
        print("Found Solution (Nr.: {}) [background knowledge ommitted]".format(idx))
        for rule_id in solution:
            print(ilasp.lookup_rule(rule_id))
    if not solution_sets:
        print("No Solution Found!")


def run_build_searchspace(ilasp, up_to_length=None):
    all_rules = ilasp.only_build_search_space(up_to_length)

    for (length, rules) in all_rules:
        for rule in rules:
            print("{} ~ {}".format(length, rule_as_str(rule)))


def main(arguments):

    with profile_section('total') as profiler_top:
        problem, config = ProblemInstance.from_config(arguments['<problem_def_file>'])
        extra_args = process_config(config)
        max_hypothesis_cost = extra_args['max_hypothesis_cost']

        logdir = setup_logdir(arguments)
        searchspace = create_search_space(arguments, problem, extra_args)
        ilasp = create_ilasp_instance(arguments, problem, searchspace)

        if arguments['solve']:
            run_solve(ilasp, max_hypothesis_cost)
        elif arguments['build_searchspace']:
            up_to_length = arguments['<up_to_length>']
            if up_to_length is not None:
                up_to_length = int(up_to_length)
            run_build_searchspace(ilasp, up_to_length)
        else:
            raise ValueError(
                "unexpected state, docopt should have ensured that this branch is not reachable,"
                "probably a misconfiguration of the doc string")

    # make sure this is not inside the with statement!
    write_profiling_data(profiler_top, logdir)


if __name__ == '__main__':
    from docopt import docopt
    arguments = docopt(__doc__, version="ilasp.py 0.1")

    if arguments['info']:
        info(arguments)
    else:
        main(arguments)