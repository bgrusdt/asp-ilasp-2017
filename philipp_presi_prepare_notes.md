
# What I need for the presi

1. alternate description
	- $\forall e^+ ~ \in E^+ \exists q \in Q: ~ extends(q, e^+)$
		- what is the donation for the set of all solutions of an ASP programm
		- see how we have a $\forall$, $\exists$
		- we can not turn the exists into a for all
	- $\forall q \in Q ~ \forall e^- \in E^-: ~ \neg extends(q, e^-)$
		- see how we have a $\forall$, $\forall$
		- two for all's so we can collaps this, even through the predicate is
		  negated
	- why do we need a copy of all rules for each example
	- why do we need another copy for neg examples
	- why do we need _just one_ copy for _all_ neg examples

2. why can't we collaps both phases into one
	- not just a intuitive solution I want a (scetch of a) profe

