import clingo


def parse_program(program_string, rename_base=None):
    program = []
    try:
        r = clingo.parse_program(program_string, program.append)
    except RuntimeError as e:
        #FIXME reraise/cause
        raise RuntimeError("can not parse program", program_string)
    assert r is None, "sanity check : clingo.parse_program returns always None"
    if rename_base is not None:
        program[0].name = str(rename_base)
    return program


def create_literal(name, const_parameters=[], negate=False):
    """create_head_atom("covered","1") -> ast covered(1) """
    return ASTLiteral(
        atom=clingo.ast.SymbolicAtom(
            term=ASTFunction(
                name=name,
                arguments=list(map(ASTSymbol, const_parameters))
            )
        ),
        negate_sign=negate
    )


def ASTConstraintHead():
    """
    :return: a clingo.ast.Literal instance for a constraint head (#false)
    """
    return ASTLiteral(
        atom=clingo.ast.BooleanConstant(value=False),
        negate_sign=False
    )


def ASTRule(head, body):
    return clingo.ast.Rule(
        location=_fake_location(),
        head=head,
        body=body
    )


def ASTProgramMarker(name):
    """
    :param name: the name of the program for with the marker is
    :return: a clingo.ast.Program  instance
    """
    location = _fake_location()
    parameters = []
    return clingo.ast.Program(name=name, location=location, parameters=parameters)


def _fake_location():
    return {'begin': {'column': 1, 'filename': '<string>', 'line': 1},
                'end': {'column': 1, 'filename': '<string>', 'line': 1}}


def ASTFunction(name, arguments=[], external=False):
    """
    :param name: the functions name
    :param arguments:  all function parameters (clingo.ast terms)
    :param external: set true if the parameter should be external (default false)
    :return: a clingo.ast.Function instance
    """
    return clingo.ast.Function(
        location=_fake_location(),
        name=name,
        arguments=arguments,
        external=external
    )


def ASTLiteral(atom, negate_sign=False):
    """
    :param atom: the atom contained in the literal
    :param negate_sign: use Negation or None sing
    :return: a clingo.ast.Literal instance
    """
    return clingo.ast.Literal(
        location=_fake_location(),
        atom=atom,
        sign=get_sign(negate=negate_sign)
    )


def get_sign(negate):
    """returns a clingo.ast Sign, either None or Negation

    :param negate: wether or not to return Negation
    :return: clingo.ast.Sing.Negation or clingo.ast.Sign.None
    """
    if negate:
        return clingo.ast.Sign.Negation
    else:
        return clingo.ast.Sign.__dict__["None"]


def ASTSymbol(symbol):
    """creates clingo.ast.Symbol

    if the symbol parameter is not a `clingo.Symbol` instance, it
    is converted to such by using `clingo.Number` or `clingo.String`.

    :param symbol: either clingo.Symbol, a number or a string
    :return: a clingo.ast.Symbol instance
    """
    symbol_type = type(symbol)
    if symbol_type is not clingo.Symbol:
        if symbol_type is int:
            symbol = clingo.Number(symbol)
        elif symbol_type is str:
            # clingo uses allways Function(name, arguments=[]) when parsing
            # clingo.String is not used, so we keep it with that
            symbol = clingo.Function(symbol)
        else:
            raise ValueError("unexpected type for clingo.Symbol: {}", symbol_type)

    return clingo.ast.Symbol(
        location=_fake_location(),
        symbol=symbol
    )


def ASTVariable(name):
    """
    :param name: the variable name
    :return: a clingo.ast.Variable instance
    """
    return clingo.ast.Variable(
        location=_fake_location(),
        name=name
    )


def atom_as_literal(atom):
    return ASTLiteral(
        atom=clingo.ast.SymbolicAtom(
            term=ASTSymbol(atom)
        )
    )


def is_function(ast):
    return ast.type == clingo.ast.ASTType.Function


def is_symbolic_atom(ast):
    return ast.type == clingo.ast.ASTType.SymbolicAtom


def walk_ast(ast_top, visitor):
    """
    Traverses the given (`ast_top`) abstract syntax tree passing
    all found instances of `clingo.ast.AST` to `visitor`. If
    `visitor(ast_node)` return `True` the branch of the ast,
    is considered done and no subnodes of `ast_node` will be
    traversed, if it returns `False` all sub nodes will be
    traversed. Modifying a `ast_node` in the `visitor` is
    possible. It is not possible to replace a `ast_node`.

    The nodes passed to `visitor` are always instances of clingo.ast.AST.

    :param ast_top: the ast node at the top of the ast(sub-)tree used for searching
    :return: a generator yielding nodes of the SymbolicAtom type
    """
    asts_to_check = [ast_top]
    while asts_to_check:
        check_ast = asts_to_check.pop()
        if type(check_ast) is list:
            asts_to_check.extend(check_ast)
        if type(check_ast) is clingo.ast.AST:
            if visitor(check_ast):
                # we only look for roots, no encapsulated SymbolicAtoms
                # so no push/append here
                pass
            else:
                asts_to_check.extend(check_ast.values())


# def find_program_names(rules):
#     """ yields the names of all Program nodes in rules
#
#     :param rules: a list of ast rules
#     :return: a generator yielding all program names appearing in rules
#     """
#     for rule in rules:
#         if is_program_maker(rule):
#             yield rule.name


def is_program_maker(rule):
    return rule.type is clingo.ast.ASTType.Program


def create_constraint(atoms):
    """create constraints without negations from a sequence of atoms"""
    return ASTRule(
        head=ASTConstraintHead(),
        body=list(map(atom_as_literal, atoms))
    )