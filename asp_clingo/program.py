
from asp_clingo.ast import  ASTProgramMarker,is_program_maker, parse_program


class Program(object):

    @classmethod
    def from_str(cls, name, string):
        ast = parse_program(string, rename_base=name)
        prog = cls(name)
        prog.extend(ast)
        return prog

    def __init__(self, name):
        self._name = name
        self._rules = [ASTProgramMarker(self._name)]

    def rules(self):
        return list(self._rules)

    def free_rules(self):
        """returns the rules without a program marker"""
        return list(self._rules[1:])

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._rules[0].name = value
        self._name = value

    def extend(self, rules_iter):
        name = self.name
        prev_len = len(self._rules)
        try:
            self._rules.extend(rule for rule in rules_iter if check_rule(rule, name))
        except ValueError as e:
            self._rules = self._rules[:prev_len]
            raise

    def add_rule(self, rule):
        self.extend([rule])


def check_rule(rule, exp_name):
    if is_program_maker(rule):
        if rule.name != exp_name:
            raise ValueError("adding ast for a different program to this program")
        return False
    return True