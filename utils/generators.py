class IDGenerator:

    def __init__(self, prefix, start_id=1):
        """

        :param prefix: the prefix to use when generating new Id's
        """
        self._current_id_num = start_id - 1
        self._prefix = prefix

    def get_currentId(self):
        """

        :return: the current used id
        """
        return self._prefix + str(self._current_id_num)

    def nextId(self):
        """
        Moves to the next id and returns it

        :return: the next id which will be used
        """
        self._current_id_num += 1
        return self._prefix + str(self._current_id_num)

    def get_current_id_number(self):
        """

        :return: the number on which the current id is based on
        """
        return self._current_id_num

    def get_prefix(self):
        """

        :return: the prefix used to generate the id
        """
        return self._prefix

    def reset_ID(self):
        """

        :return: undoes the last call to nextID
        """
        self._current_id_num -= 1



