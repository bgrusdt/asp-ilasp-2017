"""
New hierarchical profiling module

# Threading

The parent profiling section is implicitly passed through thread local storage,
_if_ a child is run in a different thread it still can connect manually to the
parent by passing the value bound in the with context to the top most profiling
section of the child thread as root parameter. Note that this measn that a child
can stop _after_ the parent does i.e. a childs duration can be grater then
the parents duration and in extension of this the childs start+duration can
be grater then the parents. Through this can only happen with threads not
bound to end in the profiling section where it started.

# Example

```

def run_program():
    with Section('rp1'):
        ...
        with Section('rp2'):
            ...
    ...
    # Note we use rp2 _AGAIN_
    with Section('rp2'):
        ...
    ...


if __name__ == '__main__':
    top = Section('total')
    with top:
        run_program()

    assert top.get_times() == Time(
        name='total',
        start=..., duration=...,
        children=[
            Time(
                name='rp1',
                start=..., duration=...,
                children=[
                    Time(
                        name='rp2',
                        start=..., duration=...,
                        children=[]
                    )
                ]
            ),
            Time(
                name='rp2',
                start=..., duration=...,
                children=[]
            )
        ]
    )
```

"""

from functools import wraps
import threading
import json
from collections import namedtuple
import time

_timeCtx = threading.local()


def profile_section(name, root=None, discard_on_exception=False, metadata=None):
    return SectionContext(name, root, discard_on_exception, metadata=metadata)


class profile_iter_next(object):
    """
    profiles the next call to an iterator,
    be aware that this literally profiles the next call and
    therefore includes one call more then there are items,
    the call which raised an StopIteration exception

    The name is the given profiling name + [<index>],
    where <index> is the enumeration index of the
    iterator (i.e. 0, 1, 2, ...)
    """

    def __init__(self, iter, prof_name, root=None):
        self.__inner = iter
        self.__count = 0
        self.__root = root
        self.__prof_name = prof_name

    def next(self):  # real signature unknown; restored from __doc__
        """ x.next() -> the next value, or raise StopIteration """
        return self.__next__()

    def __next__(self):
        count = self.__count
        self.__count = count + 1
        name = self.__prof_name + "[" + str(count) + "]"
        with profile_section(name, self.__root, discard_on_exception=True):
            return self.__inner.next()

    def __iter__(self):
        """ x.__iter__() <==> iter(x) """
        return self


def profile(name, metadata=None):
    """decorator for profiling a whole function

    This will basically wrap the function call in a
    `with profile_section(name): original_func(..)`.

    Where the name is for now _not_ auto generated.

    # Example

    @profile(name="that_function")
    def that_function():
        ...
    """

    def _annotation(func):
        @wraps(func)
        def profiled(*args, **kwargs):
            with profile_section(name, metadata=metadata) as f:
                return func(*args, **kwargs)

        return profiled

    return _annotation


class SectionContext(object):

    def __init__(self, name, root=None, discard_on_exception=False, metadata=None):
        self.name = name
        self.start = None
        self.duration = None
        self.child_times = []
        self.root = root
        self._rebind = None
        self.metadata = [] if metadata is None else metadata
        self.discard_on_exception = discard_on_exception

    def add_metadatum(self, metadatum):
        self.metadata.append(metadatum)

    def _bind_current_prof_section(self):
        self._rebind = get_active_profiling_section()
        if self.root is None:
            self.root = self._rebind

        _timeCtx.current_profiling_ctx = self

    def _unbind_current_prof_section(self):
        _timeCtx.current_profiling_ctx = self._rebind

    def __enter__(self):
        if self.start is not None:
            raise RuntimeError('profile Section context __enter__ called twice')
        self._bind_current_prof_section()
        self.start = time.time()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._unbind_current_prof_section()
        if exc_type is not None and self.discard_on_exception:
            return

        end = time.time()
        if self.start is None:
            raise RuntimeError('profile Section context __exit__ called without __enter__')
        if self.duration is not None:
            raise RuntimeError('profile Section context __exit__ called twice')
        self.duration = end - self.start

        if self.root is not None:
            self.root.child_times.append(self.get_times())

    def get_times(self):
        return Time(
            name=self.name,
            start=self.start,
            duration=self.duration,
            # we want to make sure this is not the same list object
            children=list(self.child_times),
            metadata=list(self.metadata)
        )


class Time(object):

    def __init__(self, name, start, duration, children, metadata=None):
        self.name = name
        self.start = start
        self.duration = duration
        self.children = children
        self.metadata = [] if metadata is None else metadata


class TimeEncoder(json.JSONEncoder):

    def default(self, o):
        if isinstance(o, Time):
            ser = {
                'name': o.name,
                'start': o.start,
                'duration': o.duration,
                'children': o.children
            }
            if len(o.metadata):
                ser['metadata'] = o.metadata

            return ser
        else:
            return json.JSONEncoder.default(self, o)


def get_active_profiling_section():
    return getattr(_timeCtx, 'current_profiling_ctx', None)
