from collections import namedtuple
import json
import utils

from logger import IlaspLogger

StrRule = namedtuple('StrRule', ['prg_name', 'prg_vars', 'prg_content_string'])
LOGGER = IlaspLogger.getLogger(__name__)


def extract_actives(model):
    """
    extract `active(...)` atoms from the models answer set

    :param model:
    :return: a iterator over `active(...)` atoms
    """
    return (atom for atom in model.symbols(atoms=True) if atom.name == "active")


def extract_active_rule_ids(model, logger):
    """
    :param Model model: a model retrieved from e.g. by `solve_iter()` or `solve(on_model=...)`
    :return frozenset of rule ids (strings)
    :rtype frozenset[str]
    """
    rule_ids = []
    for active in extract_actives(model):
        if len(active.arguments) != 1:
            logger.warn("skipping malformaed active atom: {}".format(str(active)))
        else:
            rule_ids.append(str(active.arguments[0]))

    return frozenset(rule_ids)


def replace_pattern_in_predicates(predicate_list, vars_to_add, pattern):
    """
    # replaces '$v$ in each element in 'predicate_list' by variables specified in vars in the order they appear
    # length of 'vars_to_add' and the number of $v$ in 'predicate_list' must be equal
    :param predicate_list: list of strings representing each one predicate
    :param vars_to_add: list of strings which are the variables that will replace VAR_PATTERN inside elements
    of 'predicate_list'
    :return list of str replaced_preds
    """
    replaced_preds = []
    idx_vars = 0
    for literal in predicate_list:
        nbVars = literal.count(pattern)
        if nbVars == 0:
            replaced_preds.append(literal)
            continue
        replacements = vars_to_add[idx_vars: idx_vars + nbVars]
        idx_vars += nbVars
        replaced_preds.append(utils.replace(literal, pattern, replacements))

    # LOGGER.debug('replaced bodies: %s for %s with %s.', replaced_preds, predicate_list, vars_to_add)
    return replaced_preds


class TypeManager:

    @classmethod
    def from_string(cls, string):
        const_dict = {}
        consts = json.loads(string)
        #FIXME isn't it the same type? why not just clone? (and do so in constructor)
        const_dict.update(consts.iteritems())
        return cls( const_dict )

    def __init__(self, consttypes):
        self._const_typeVal_dict = consttypes
        self._constant_prefix = 'C'

    def get_ctype_val_dict(self):
        # TODO: deep copy
        return self._const_typeVal_dict

    def get_constant_prefix(self):
        return self._constant_prefix
