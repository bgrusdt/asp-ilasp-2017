import logging
import logging.config
import os.path as path


class IlaspLogger(object):
    __slots__ = ['_logger']

    @staticmethod
    def global_setup(logdir="./logs", log_level_name="WARNING"):
        """
        has to be called before logging can be used
        :param logdir: the dir to place logs in
        """
        # This will load the config and set a internal global variable
        logging.config.fileConfig(
            path.join('resources', 'logging-config.conf'),
            defaults={"log_dir": logdir, "log_level": log_level_name}
        )

    @staticmethod
    def getLogger(name):
        return IlaspLogger(name)

    def __init__(self, name):
        self._logger = logging.getLogger(name)

    def __getattr__(self, item):
        return self._logger.__getattribute__(item)

    def log_relevant_solution_preds(self, ignored_preds, list_model_strs):
        for i, model in enumerate(list_model_strs):
            self.debug('model nb %s:', i)
            model_preds = model.split(" ")
            for pred in model_preds:
                if not pred.startswith(ignored_preds):
                    self.debug('%s', pred)
