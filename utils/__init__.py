import collections
# import cProfile
# import json

from collections import defaultdict
from itertools import chain



def update_profile_dict(datadict, key, val):
    datadict[key] = val


def do_if(if_predicate, do_think):
    """

    :param Function if_predicate:
    :param Function do_think:
    :return:
    """
    def closure(value):
        if if_predicate(value):
            do_think(value)
            return True
        else:
            return False
    return closure


#FIXME potential logical duplicate with get_duplocate_indices
def get_duplicates(lst):
    """returns a dictionary from the duplicate value (completeley identical) to a list of indices where the respective duplicate
    appeared. If there are no duplicates, an empty dictionary is returned.
    :param lst: list of Anything
    """
    cnt = collections.Counter(lst)
    duplicates = [key for key in cnt.keys() if cnt[key] > 1]

    if duplicates:
        duplicate_to_indices_dict = collections.defaultdict(list)
        for i, val in enumerate(lst):
            if val in duplicates:
                duplicate_to_indices_dict[val].append(i)
    else:
        duplicate_to_indices_dict = {}

    return duplicate_to_indices_dict


def get_all_indices_by_value(queried_list, values):
    """
    :param queried_list:
    :param values:
    :return dictionary from items in values to list of their occurence-indices in queried_list
    """
    matches = defaultdict(list)
    for idx, elem in enumerate(queried_list):
        # TODO CHECK can values be a set, if so use `if elem in values: matches[item].append(idx)`
        for item in values:
            if item == elem:
                matches[item].append(idx)

    return matches


def get_list_elems_by_tuple(list_queried, idx_tuple):
    return [list_queried[idx] for idx in idx_tuple]


def put_list_elems_by_tuple(list_to_modify, src_indices, values):
    """list_to_modify is copied and content at all indices inside idx_tuple is replaced by values
    :param idx_tuple: tuple of any length < len(list_to_modify) containing indices (int)
    :param values: one single value of any type, will be put into list_to_modify at all indices in idx_tuple or:
    it can be a list of several values of any type
    """
    copied_list = list(list_to_modify)

    #FIXME CHECK do we really need to overload this function runtime based
    if not isinstance(values, list) and not isinstance(values, tuple):
        for src_idx in src_indices:
            copied_list[src_idx] = values
        return copied_list

    for idx_of_new, src_idx in enumerate(src_indices):
        copied_list[src_idx] = values[idx_of_new]

    return copied_list


#FIXME ADD a compare str_compare_asp_rule which
# 1. removes spaces
# 2. split head body
# 3. split "," on heads/bodies
# 4. => (set(heads), set(bodies))
def remove_spaces(string):
    return "".join(string.split())


def replace(string, pattern, replacements):
    """Replaces the ith occurence of 'pattern' in string by the ith element in replacements.

    :param pattern: string.
    :param replacements: list of strings.
    :raises IndexError if the number of occurences of pattern in string mismatches the len of replacements
    """
    parts = []
    len_patter = len(pattern)
    offset = 0
    found_count = 0
    while True:
        idx = string.find(pattern, offset)
        if idx < 0:
            break
        parts.append(string[offset:idx])
        offset = idx + len_patter
        parts.append(replacements[found_count])
        found_count += 1

    parts.append(string[offset:])

    if found_count != len(replacements):
        raise IndexError("count of pattern occurences in string does not match len(replacements): {} != {}"
                         .format(found_count, len(replacements)))
    # ''.join() is the fastest way to create a string from parts
    # it is (was?*) more efficient then e.g. doing x="",while .. x+=...
    # *through that case got some optimizations I think, still
    return ''.join(parts)


#FIXME same as get_list_elements_by_tuple
def index_multiple(collection, indices):
    """
    returns a list with one value retrieved from list_queried
    for each index in indices

    It is equivalent to: `tuple(collection[idx] for idx in indices)`

    :param collection: a indexable collection
    :param indices:  the indices for the values which should be retrieved from the input collection
    :return: a list of values
    """
    return [collection[idx] for idx in indices]


#FIXME duplicate with get_duplicates, only used in test
def get_indices_for_duplicates(ordered_collection):
    """
    returns the _enumeration_ index of each element occurring more than one time in ordered_collection

    :param ordered_collection: any ordered/iteration order stable collection
    :return: list of enumeration indices (sorted)
    """
    counter = collections.Counter(ordered_collection)
    return [idx for (idx, value) in enumerate(ordered_collection) if counter[value] > 1]


#FIXME duplicate with flatten
def flattened_iter(x):
    """
    Example: `assert list(flattened_iter([[1],[2,3,4],[5,6]])) == [1,2,3,4,5,6]`
    :param x:
    :return:
    """
    return chain.from_iterable(x)
