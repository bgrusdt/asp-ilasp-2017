import clingo
from collections import defaultdict
from collections import namedtuple
from logger import IlaspLogger

LOGGER = IlaspLogger.getLogger(__name__)


class PredicateAccessor:

    def __init__(self, list_predicates):
        """
        creates a member variable 'predicates' that is a list of namedtuples, with fields:
        names: list of strings; e.g. [value, value, cell] for list_predicates: [value(V,V), value(T,X), cell(W)]
        variables: list of list of strings (one for each predicate in the predicates) representing variables appearing in
        that predicate, e.g. [[V,V],[T,X],[W]] for example above

        :param list_predicates: predicates of ONE body, predicates, must be safe!
        :type list_predicates: list of strings
        """

        predicates = []
        clingo.parse_program(":- " + ",".join(list_predicates) + ".",
                             lambda stm: predicates.append(self.__extract_predicates(stm)))
        # returned list of lists contains one single treated predicate and might contain empty lists (for #program base. parts)
        new_predicates = [elem for elem in predicates if elem]

        self.list_predicates = list_predicates[:]
        self.predicates = new_predicates[0]

    def __extract_predicates(self, stm):
        sttype = str(stm.type)
        if sttype == "Rule":
            Pred = namedtuple('Pred', 'names arguments')
            predicates = []
            arguments = []
            astlist = stm.body
            for ast in astlist:
                predicates.append(ast.atom.term.name)
                arguments.append(ast.atom.term.arguments)

            return Pred(names=predicates, arguments=arguments)

        else:
            return []

    def extract_indices_duplicate_preds(self):
        """
        :return list of lists containing each the indices of duplicate predicates, e.g. [[0,1],[3,4]] for
        self.list_prdicates = [value(_,_), value(_,_), block(_,_), cell(_,_), cell(_,_)]. This is independent
        of the arguments of the predicates as long as they have the same number of arguments: value(V,V) is NOT treated
        as duplicate of value(V,V,V).
        """
        # get indices of duplicate predicates
        duplicate_indices = []

        duplicate_idx_dict = defaultdict(list)
        for idx in range(0, len(self.predicates.names)):
            name = self.predicates.names[idx]
            nbargs = len(self.predicates.arguments[idx])
            duplicate_idx_dict[(name, nbargs)].append(idx)

        for key in duplicate_idx_dict.keys():
            indices = duplicate_idx_dict[key]
            if len(indices) > 1:
                duplicate_indices.append(indices)

        return duplicate_indices

    def extract_variables(self):
        """
        @return list of lists of AST objects: representing variables of each predicate in self.list_predicates, e.g.
        [[V,V],[T,X],[W]] for list_predicates = [value(V,V), value(T,X), cell(W)]
        """
        variables = []
        # (list of lists containing clingo Symbols)
        args = self.predicates.arguments
        for args_current_pred in args:
            varlist = []
            for arg in args_current_pred:
                queue = []
                if str(arg.type) == 'Function':
                    queue += arg.arguments
                else:
                    if str(arg)[0].isupper():
                        varlist.append(arg)

                while queue:
                    item = queue[0]
                    if str(item.type) == 'Function':
                        queue += item.arguments
                    else:
                        if str(item)[0].isupper():
                            varlist.append(item)

                    queue = queue[1:]

            variables.append(varlist)

        if not any(variables):
            return []
        return variables

    def get_vars_str(self):
        """
        :return list of list of str with variables of this entire body
        """
        str_vars = []
        variables = self.extract_variables()
        for pred_vars in variables:
            vars_current_pred = []
            for ast_var in pred_vars:
                vars_current_pred.append(str(ast_var))
            str_vars.append(vars_current_pred)

        return str_vars

    def extract_clingo_arguments(self):
        # TODO: deep copy?
        return self.predicates.arguments

    def extract_pred_names(self):
        """
        :return list of str predicate names:
        e.g. [value, value, cell] for self.list_predicates = [value(V,V), value(T,X), cell(W)]
        """
        return self.predicates.names


if __name__ == '__main__':
    body = ['value(C,V)', 'value(C,V)']
    pa = PredicateAccessor(body)
    duplicate_indices = pa.extract_indices_duplicate_preds()
    # 'not same_block(C,C,tr)', 'same_block(C,V,tl)'])

    print body
    if(not duplicate_indices):
        print('no duplicates.')

    for elem in duplicate_indices:
        print elem
