from ilasp import *

RANDOM_RULES = [
    "a(X):-b(X).",
    "b(1..2).",
    "e(R):-A(X)."
]

class FakeProg(object):

    def __init__(self):
        # This is basically public but I use _ because
        # it's stubbing a think which might or might not
        # have a rules field
        self._rules = []

    def add(self, prog, vars, rule):
        self._rules.append((prog, vars, rule))

    def add_many(self, prog, vars, rules):
        for rule in rules:
            self.add(prog, vars, rule)
