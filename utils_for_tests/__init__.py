import json

from asp_representation.modes import ModeDeclaration
from data import ProblemInstance
from utils.ilasp_utils import StrRule

PATH_TEST_MODES = './resources/test-files/mode_declarations.json'
PATH_TEST_CONFIG = './resources/test-files/ilasp.properties'


def rules(prog_name, rules):
    return [StrRule(prog_name, [], rule) for rule in rules]

def rules_with_param(prog_name, param, rules):
    return [StrRule(prog_name, param, rule) for rule in rules]

def as_str_set(listing):
    return set(map(str, listing))


def is_str(value):
    return type(value) is str


def expect_failure(code, expected):
    try:
        code()
    except expected as e:
        pass
    else:
        raise AssertionError("no exception was rosen even through {} was expected".format(expected))


def get_custom_mode(self, vtypes, ctypes, mtype, content):
    m0 = ModeDeclaration({'content': content,
                          'vartypes': vtypes,
                          'consttypes': ctypes},
                         mtype)
    return m0


def get_mode_declarations_from_file():
    mode_dict = read_mode_file()
    modes_b = setup_mode_declarations_dict(mode_dict, 'M_b')
    modes_h = setup_mode_declarations_dict(mode_dict, 'M_h')
    modes_ha = setup_mode_declarations_dict(mode_dict, 'M_ha')

    modes_updated = dict(modes_b, **modes_h)
    modes_updated.update(modes_ha)
    return modes_updated


def get_type_manager_from_config():
    (problem, _) = ProblemInstance.from_config(PATH_TEST_CONFIG)
    return problem.type_manager


def read_mode_file():
    """
    :param str mtype: one of 'M_b', 'M_h' or 'M_ha'
    """
    with open(PATH_TEST_MODES) as json_data:
        try:
            mode_dict = json.load(json_data)
        except Exception, e:
            message = '''The ModeDeclarations file must follow the structure specified in the Readme File.'''
            raise ValueError(message + '\n' + str(e))
    return mode_dict


def setup_mode_declarations_dict(mode_dict, mtype):
    mode_declarations = {mtype: []}
    for item in mode_dict[mtype]:
        mode_declarations[mtype].append(ModeDeclaration(item, mtype))
    return mode_declarations


