'''
Created on Oct 18, 2017

@author: britta
'''
import unittest

from searchspace.bodies import Body
from searchspace.heads import Head
from searchspace.rules import CandidateRulePart
from searchspace.rules import convert_rule_to_meta
from searchspace.rules import generate_rule_str
from utils import remove_spaces
from utils_for_tests import get_mode_declarations_from_file
from utils_for_tests import get_type_manager_from_config


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.type_manager = get_type_manager_from_config()
        cls.MODE_DECLARATIONS = get_mode_declarations_from_file()

    def test_convert_rule_to_meta_constraint(self):
        body = Body(('m0', 'm0'), ['a(V0,V1)', 'a(V0,V1)'], vtype_dict={'V0': 'cell', 'V1': 'value'})
        meta = convert_rule_to_meta('r0', body)
        self.assertEqual(remove_spaces(meta), ':-e(a(V0,V1),X),e(a(V0,V1),X),active(r0),ex(X).')

    def test_convert_rule_to_meta_simple_head(self):
        # TODO: heads with constants
        body = Body(('m0', 'm0'), ['a(V0,V1)', 'a(V0,V1)'], vtype_dict={'V0': 'cell', 'V1': 'value'})
        head = Head(['b(V0,V1)'])
        meta = convert_rule_to_meta('r0', body=body, head=head)
        self.assertEqual(remove_spaces(meta), 'e(b(V0,V1),X):-e(a(V0,V1),X),e(a(V0,V1),X),active(r0),ex(X).')

    def test_convert_rule_to_meta_aggregate(self):
        body = Body(('m0', 'm0'), ['a(V0,V1)', 'a(V0,V1)'], vtype_dict={'V0': 'cell', 'V1': 'value'})
        head = Head(['r(V0,c1,c1)', 'r(V0,c1,c2)'], aggregate=True, agg_bounds=(1, 1))
        meta = convert_rule_to_meta('r1', body, head)
        self.assertEqual(remove_spaces(meta),
                         '1{e(r(V0,c1,c1),X);e(r(V0,c1,c2),X)}1:-e(a(V0,V1),X),e(a(V0,V1),X),active(r1),ex(X).')

    def test_generate_rule_str(self):
        body = Body(('m0', 'm0'), ['a(V0,V1)', 'a(V1,V0)'], vtype_dict={'V0': 'cell', 'V1': 'cell'})
        head1 = Head(['b(V0,V1)'])
        rule1 = generate_rule_str(body=body, head=head1)
        self.assertEqual(remove_spaces(rule1), 'b(V0,V1):-a(V0,V1),a(V1,V0).')

        head2 = Head(['b(V0)', 'c(V1)'], aggregate=True, agg_bounds=(1, 2))
        rule2 = generate_rule_str(body, head2)
        self.assertEqual(remove_spaces(rule2), '1{b(V0);c(V1)}2:-a(V0,V1),a(V1,V0).')

    # Class CandidateRulePart
    def test_get_abstraction(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        # 0 1 2 2; m1 m2 m3 m3
        body_candidate = CandidateRulePart([modes[0], modes[1], modes[3], modes[3]], Test.type_manager)
        abstraction = body_candidate.get_abstraction()
        self.assertEqual(abstraction, ((0, 'm0'), (1, 'm1'), (2, 'm2'), (2, 'm2')))

    def test_get_anti_ref_indices(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart(modes, Test.type_manager)
        anti_refs = body_candidate.get_anti_ref_indices()
        self.assertEqual(anti_refs, [0, 2, 6, 7, 8])

    def test_get_combi(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart(modes, Test.type_manager)
        combi = body_candidate.get_combi()
        self.assertEqual(combi, ('m1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9', 'm10', 'm11'))

    def test_get_constant_combis(self):
        modes = Test.MODE_DECLARATIONS['M_ha']
        body_candidate = CandidateRulePart([modes[0], modes[0], modes[0]], Test.type_manager)
        const_combis = set(body_candidate.get_constant_combis())
        self.assertEqual(const_combis, set([('c1', 'c1', 'c1'), ('c1', 'c1', 'c2'), ('c1', 'c2', 'c1'), ('c2', 'c1', 'c1'),
                                            ('c2', 'c2', 'c2'), ('c2', 'c2', 'c1'), ('c2', 'c1', 'c2'), ('c1', 'c2', 'c2')]))

    def test_get_const_indices(self):
        modes = Test.MODE_DECLARATIONS['M_ha']
        body_candidate = CandidateRulePart([modes[0], modes[1], modes[0]], Test.type_manager)
        const_indices = body_candidate.get_const_indices([2, 1, 0])
        self.assertEqual(const_indices, [[3], [1, 2], [0]])

    def test_get_duplicates_dict(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[0], modes[1], modes[1], modes[2], modes[3], modes[3]], Test.type_manager)
        dd = body_candidate.get_duplicates_dict()
        self.assertEqual(dd, {'m2': [1, 2], 'm4': [4, 5]})

    def test_get_duplicate_pred_ids(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[0], modes[1], modes[1], modes[2], modes[3], modes[3]], Test.type_manager)
        dup_ids = set(body_candidate.get_duplicate_pred_ids())
        self.assertEqual(dup_ids, set(['m2', 'm4']))

    def test_get_duplicate_indices(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[0], modes[1], modes[1], modes[2], modes[3], modes[3]], Test.type_manager)
        dup_indices = set([tuple(l) for l in body_candidate.get_duplicate_indices()])
        self.assertEqual(dup_indices, set([(1, 2), (4, 5)]))

    def test_get_nb_consts_per_pred(self):
        modes = Test.MODE_DECLARATIONS['M_ha']
        body_candidate = CandidateRulePart([modes[0], modes[1], modes[0]], Test.type_manager)
        nb_consts = body_candidate.get_nb_consts_per_pred()
        self.assertEqual(nb_consts, [1, 2, 1])

    def test_get_nb_vars(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[1], modes[2], modes[5], modes[1]], Test.type_manager)
        nb_vars = body_candidate.get_nb_vars()
        self.assertEqual(nb_vars, 7)

    def test_get_nb_vars_per_pred(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[1], modes[2], modes[5], modes[1]], Test.type_manager)
        nb_vars_per_pred = body_candidate.get_nb_vars_per_pred()
        self.assertEqual(nb_vars_per_pred, [1, 2, 3, 1])

    def test_get_original_content(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[1], modes[2], modes[5], modes[1]], Test.type_manager)
        original_content = body_candidate.get_original_content()
        self.assertEqual(original_content, ['q($V$)', 'r($V$,$V$)', 'u($V$,$V$,$V$)', 'q($V$)'])

    def test_get_positive_indices(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[0], modes[1], modes[2], modes[3], modes[4], modes[5], modes[6], modes[7],
                                            modes[1], modes[3]], Test.type_manager)
        positives = body_candidate.get_positive_indices()
        self.assertEqual(positives, [0, 3, 4, 9])

    def test_get_symetric_ids(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[0], modes[1], modes[2], modes[3], modes[4], modes[5], modes[6], modes[7],
                                            modes[9], modes[10]], Test.type_manager)
        symetrics = body_candidate.get_symetric_ids()
        self.assertEqual(symetrics, ['m4', 'm10'])

    def test_get_var_indices(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[0], modes[7], modes[1], modes[10]], Test.type_manager)
        var_indices = body_candidate.get_var_indices([0, 1, 2, 3])
        self.assertEqual(var_indices, [[], [0], [1], [2, 3]])

    def test_get_var_indices_dict(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[0], modes[7], modes[1], modes[10], modes[10]], Test.type_manager)
        var_indices_dict = body_candidate.get_var_indices_dict()
        self.assertEqual(var_indices_dict, {'m1': [[]], 'm8': [[0]], 'm2': [[1]], 'm11': [[2, 3], [4, 5]]})

    def test_get_vtypes(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[0], modes[7], modes[1], modes[10], modes[10]], Test.type_manager)
        vtypes = body_candidate.get_vtypes()
        self.assertEqual(vtypes, ['t1', 't1', 't1', 't1', 't1', 't1'])

if __name__ == "__main__":
    unittest.main()
