'''
Created on Sep 26, 2017

@author: britta
'''
import unittest
from collections import defaultdict
from testfixtures import log_capture

from searchspace.bodies import Body
from searchspace.rules import CandidateRulePart
from searchspace.tricks import check_anti_reflexive
from searchspace.tricks import check_types
from searchspace.tricks import check_symmetry
from searchspace.tricks import check_global_symmetry
from searchspace.tricks import check_body_duplicates
from utils_for_tests import get_mode_declarations_from_file
from utils_for_tests import get_type_manager_from_config


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.type_manager = get_type_manager_from_config()
        cls.MODE_DECLARATIONS = get_mode_declarations_from_file()

    def test_check_anti_reflexive(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        # ar1(V,V), nar1(V1), nar2(V2)
        reject = check_anti_reflexive(CandidateRulePart([modes[2], modes[1], modes[1]], Test.type_manager),
                                      ['T0', 'T0', 'T2', 'T0'])
        self.assertEqual(reject, True)

        # nar(V1,V2), nar2(V1,V3)
        reject = check_anti_reflexive(CandidateRulePart([modes[3], modes[4]], Test.type_manager),
                                      ['V1', 'V2', 'V1', 'V3'])
        self.assertEqual(reject, False)

        # nar(V1,V2), nar2(V1,V3, V4), ar(V0,V1), nar3(V4), ar2(V1, V1)
        reject = check_anti_reflexive(CandidateRulePart([modes[4], modes[5], modes[2], modes[1], modes[2]],
                                                        Test.type_manager),
                                      ['V1', 'V2', 'V1', 'V3', 'V4', 'V0', 'V1', 'V4', 'V1', 'V1'])
        self.assertEqual(reject, True)

        # nar(V1,V2,V3)
        reject = check_anti_reflexive(CandidateRulePart([modes[5]], Test.type_manager), ['V0', 'V1', 'V2'])
        self.assertEqual(reject, False)

        # "ar"(V1,V2,V3)
        # Warning in LOG-File!
        reject = check_anti_reflexive(CandidateRulePart([modes[6]], Test.type_manager), ['V0', 'V1', 'V2'])
        self.assertEqual(reject, False)

    @log_capture()
    def test_function(self, l):
        modes = Test.MODE_DECLARATIONS['M_b']
        check_anti_reflexive(CandidateRulePart([modes[6]], Test.type_manager), ['V0', 'V1', 'V2'])
        l.check(
            ('searchspace.tricks', 'WARNING', 'anti-reflexive predicates must be 2-ary.'),
            )

        l.uninstall()
        check_anti_reflexive(CandidateRulePart([modes[7]], Test.type_manager), ['V0'])
        l.check(
            ('searchspace.tricks', 'WARNING', 'anti-reflexive predicates must be 2-ary.'),
            )

    def test_check_types_kept(self):
        variables = ['V0', 'V1', 'V0']
        vtypes = ['cell', 'number', 'cell']
        reject, vt_dict = check_types(variables, vtypes)

        self.assertEqual(reject, False)
        self.assertEqual(vt_dict, {'V0': 'cell', 'V1': 'number'})

    def test_check_types_reject(self):
        variables = ['V0', 'V1', 'V0']
        vtypes = ['cell', 'number', 'noncell']
        reject, vt_dict = check_types(variables, vtypes)

        self.assertEqual(reject, True)
        self.assertEqual(vt_dict, defaultdict(str))

    def test_check_symmetry_kept(self):
        modes = Test.MODE_DECLARATIONS['M_b']

        # sym dups: {'m0': [(0, 1), (2, 3)]}
        body_candidate = CandidateRulePart([modes[8], modes[8], modes[7]], Test.type_manager)
        reject = check_symmetry(body_candidate, ['V0', 'V1', 'V2', 'V1', 'V0'])
        self.assertEqual(reject, False)

        # sym dups: {'m0': [(0, 1), (2, 3)], 'm1': [(4, 5)]}
        body_candidate = CandidateRulePart([modes[8], modes[8], modes[3]], Test.type_manager)
        reject = check_symmetry(body_candidate, ['V0', 'V1', 'V2', 'V1', 'V1', 'V0'])
        self.assertEqual(reject, False)

    def test_check_symmetry_reject(self):
        modes = Test.MODE_DECLARATIONS['M_b']

        # sym dups: {'m0': [(0, 1), (3, 4)]}
        body_candidate = CandidateRulePart([modes[8], modes[1], modes[8]], Test.type_manager)
        reject = check_symmetry(body_candidate, ['V0', 'V1', 'V2', 'V1', 'V0'])
        self.assertEqual(reject, True)

        # sym dups: {'m0': [(0, 1), (2, 3), (4, 5)]}
        body_candidate = CandidateRulePart([modes[8], modes[8], modes[8]], Test.type_manager)
        reject = check_symmetry(body_candidate, ['V0', 'V1', 'V2', 'V1', 'V1', 'V0'])
        self.assertEqual(reject, True)

        # sym dups: {'m0': [(0, 1), (2, 3)], 'm1': [(4, 5), (6, 7)]}
        body_candidate = CandidateRulePart([modes[8], modes[8], modes[3], modes[3]], Test.type_manager)
        reject = check_symmetry(body_candidate, ['V0', 'V1', 'V2', 'V1', 'V1', 'V0', 'V0', 'V1'])
        self.assertEqual(reject, True)

#         with self.assertRaises(ValueError):
#             check_symmetry({'m0': [(0,)]}, ['V0'])
# 
#         with self.assertRaises(ValueError):
#             check_symmetry({'m0': [(3, 4), (0, 1, 2)]}, ['V0'])

    def test_chek_rule_internal_equal_vars(self):
        body = Body(('m0', 'm0'), ['a(V0,V1)', 'a(V0,V1)'], vtype_dict={'V0': 'cell', 'V1': 'value'})
        keep = check_body_duplicates(body)
        self.assertEqual(keep, None)

    def test_chek_rule_internal_no_dups(self):
        body = Body(('m0', 'm1'), ['a(V0, V1)', 'b(V0,V1)'], vtype_dict={'V0': 'cell', 'V1': 'value'})
        keep = check_body_duplicates(body)
        self.assertEqual(keep, [])

    def test_chek_rule_internal_dup_with_single_var_reject(self):
        body = Body(('m0', 'm0'), ['a(V0, V0)', 'a(V0,V1)'], vtype_dict={'V0': 'cell', 'V1': 'value'})
        keep = check_body_duplicates(body)
        self.assertEqual(keep, [[0, 1]])

    def test_chek_rule_internal_dup_with_single_var_keep(self):
        body = Body(('m0', 'm0', 'm1'), ['a(V0, V0)', 'a(V0,V1)', 'b(V1)'], vtype_dict={'V0': 'cell', 'V1': 'value'})
        keep = check_body_duplicates(body)
        self.assertEqual(keep, [])

    def test_chek_rule_internal(self):
        b0 = Body(('m0', 'm0'), ['a(V0, V1)', 'a(V0,V2)'], vtype_dict={'V0': 'cell', 'V1': 'cell', 'V2': 'cell'})
        keep = check_body_duplicates(b0)
        self.assertEqual(keep, [[0, 1]])

        b1 = Body(('m0', 'm0'), ['a(V0, V1)', 'a(V2,V0)'], vtype_dict={'V0': 'cell', 'V1': 'cell', 'V2': 'cell'})
        keep = check_body_duplicates(b1)
        self.assertEqual(keep, [])

    def test_check_global_symmetry(self):
        modes = Test.MODE_DECLARATIONS['M_b']
        # m0 = self._get_custom_mode(['cell', 'value'], [], 'modeb', 'modeb(2, a($V$, $V$), (symmetric,))')
        # m1 = self._get_custom_mode(['cell'], [], 'modeb', 'modeb(1, b($V$,a))')

        body_candidate = CandidateRulePart([modes[9], modes[1]], Test.type_manager)
        valid_body = Body(('m9', 'm1'), ['y(V0, V1)', 'q(V0)'], vtype_dict={'V0': 't1', 'V1': 'value'})
        forbidden_rules = check_global_symmetry(valid_body, body_candidate)
        # only abstracted rules are returned --> a(V1,V0),b(V0,a) results in a(V0,V1)b(V1,a)
        expected_rules = {'y(V0,V1),q(V1)'}
        self.assertEqual(forbidden_rules, expected_rules)

if __name__ == "__main__":
    unittest.main()
