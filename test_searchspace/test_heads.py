'''
Created on Oct 24, 2017

@author: britta
'''
import unittest

from searchspace.bodies import Body
from searchspace.heads import build_aggregate_bounds
from searchspace.heads import build_head_var_combis
from searchspace.heads import generate_single_heads
from searchspace.heads import MetaAgg
from utils_for_tests import get_mode_declarations_from_file
from utils_for_tests import get_type_manager_from_config


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.type_manager = get_type_manager_from_config()
        cls.MODE_DECLARATIONS = get_mode_declarations_from_file()

    def test_build_aggregate_bounds(self):
        bounds = build_aggregate_bounds(max_rule_length=24)
        for agg_length, dictionary in bounds.iteritems():
            for head_literals, val_list in dictionary.iteritems():
                bounds[agg_length][head_literals] = set(val_list)

        expected = {24: {3: set([MetaAgg(bounds=(0, 3))]), 4: set([MetaAgg(bounds=(2, 2))])},
                    21: {3: set([MetaAgg(bounds=(1, 3)), MetaAgg(bounds=(0, 2))])},
                    12: {3: set([MetaAgg(bounds=(2, 3)), MetaAgg(bounds=(0, 1))])},
                    18: {3: set([MetaAgg(bounds=(1, 2))])},
                    9: {3: set([MetaAgg(bounds=(1, 1)), MetaAgg(bounds=(2, 2))])},
                    8: {2: set([MetaAgg(bounds=(0, 2))])},
                    6: {2: set([MetaAgg(bounds=(1, 2)), MetaAgg(bounds=(0, 1))])},
                    4: {2: set([MetaAgg(bounds=(1, 1))])},
                    2: {1: set([MetaAgg(bounds=(0, 1))])},
                    20: {4: set([MetaAgg(bounds=(0, 1)), MetaAgg(bounds=(3, 4))])},
                    16: {4: set([MetaAgg(bounds=(1, 1)), MetaAgg(bounds=(3, 3))])}
                    }
        self.assertEqual(bounds, expected)

    def test_build_aggregate_bounds_restricted(self):
        bounds = build_aggregate_bounds(max_rule_length=24, minh=2, maxh=3)
        for agg_length, dictionary in bounds.iteritems():
            for head_literals, val_list in dictionary.iteritems():
                bounds[agg_length][head_literals] = set(val_list)

        expected = {24: {3: set([MetaAgg(bounds=(0, 3))])},
                    21: {3: set([MetaAgg(bounds=(1, 3)), MetaAgg(bounds=(0, 2))])},
                    12: {3: set([MetaAgg(bounds=(2, 3)), MetaAgg(bounds=(0, 1))])},
                    18: {3: set([MetaAgg(bounds=(1, 2))])},
                    9: {3: set([MetaAgg(bounds=(1, 1)), MetaAgg(bounds=(2, 2))])},
                    8: {2: set([MetaAgg(bounds=(0, 2))])},
                    6: {2: set([MetaAgg(bounds=(1, 2)), MetaAgg(bounds=(0, 1))])},
                    4: {2: set([MetaAgg(bounds=(1, 1))])}
                    }
        self.assertEqual(bounds, expected)

    def test_build_head_var_combis_several_body_vars_multiple_head_vars_disallowed_body_sym(self):
        # q(V,V) :- a(V0,V1) symmetric
        combis = build_head_var_combis(['V0', 'V1'], {'V0': 'V1', 'V1': 'V0'}, [2], True)
        self.assertTrue((('V0', 'V0') in combis and not ('V1', 'V1') in combis) or
                        (('V1', 'V1') in combis and not ('V0', 'V0') in combis))
        self.assertTrue(len(combis) == 1)

    def test_build_head_var_combis_several_body_vars_multiple_head_vars_disallowed_no_body_sym(self):
        # q(V,V) :- a(V0,V1)
        combis = build_head_var_combis(['V0', 'V1'], {}, [2], True)
        self.assertTrue(('V0', 'V0') in combis and ('V1', 'V1') in combis)

    def test_build_head_var_combis_several_body_vars_multiple_head_vars_allowed_body_sym(self):
        # q(V,V) :- a(V0,V1)
        combis = build_head_var_combis(['V0', 'V1'], {'V0': 'V1', 'V1': 'V0'}, [2], False)
        self.assertTrue((('V0', 'V0') in combis and not ('V1', 'V1') in combis) or
                        (('V1', 'V1') in combis and not ('V0', 'V0') in combis))
        self.assertTrue((('V0', 'V1') in combis and not ('V1', 'V0') in combis) or
                        (('V1', 'V0') in combis and not ('V0', 'V1') in combis))
        self.assertTrue(len(combis) == 2)

    def test_build_head_var_combis_several_body_vars_multiple_head_vars_allowed_no_body_sym(self):
        # q(V,V) :- a(V0,V1)
        combis = build_head_var_combis(['V0', 'V1'], {}, [2], False)
        self.assertTrue(('V0', 'V0') in combis and ('V1', 'V1') in combis and
                        ('V0', 'V1') in combis and ('V1', 'V0') in combis)
        self.assertTrue(len(combis) == 4)

    def test_build_head_var_combis_several_body_vars_unique_head_var_body_sym(self):
        # q(V) :- a(V0,V1) symmetric
        combis = build_head_var_combis(['V0', 'V1'], {'V0': 'V1', 'V1': 'V0'}, [1], False)
        self.assertTrue((('V0',) in combis and not ('V1',) in combis) or
                        (('V1',) in combis and not ('V0',) in combis))
        combis = build_head_var_combis(['V0', 'V1'], {'V0': 'V1'}, [1], True)
        self.assertTrue((('V0',) in combis and not ('V1',) in combis) or
                        (('V1',) in combis and not ('V0',) in combis))

    def test_build_head_var_combis_several_body_vars_unique_head_var_body_no_sym(self):
        # q(V) :- a(V0,V1)
        combis = build_head_var_combis(['V0', 'V1'], {}, [1], False)
        self.assertTrue(('V0',) in combis and ('V1',) in combis)
        combis = build_head_var_combis(['V0', 'V1'], {}, [1], True)
        self.assertTrue(('V0',) in combis and ('V1',) in combis)

    def test_generate_single_heads_several_consts(self):
        modes = Test.MODE_DECLARATIONS['M_h']
        body = Body(('m1', 'm8'), ['q(V0)', 'v(V1)'], vtype_dict={'V0': 't1', 'V1': 't1'})
        heads = generate_single_heads([modes[3]], body, Test.type_manager, False)
        contents = set([(item.head.get_content()[0], tuple(item.extra_bodies)) for item in heads])
        self.assertEqual(contents, {('pred(V0,C0,C1)', ('t1(C0)', 't1(C1)')), ('pred(V1,C0,C1)', ('t1(C0)', 't1(C1)'))})

    def test_generate_single_heads_several_vars(self):
        modes = Test.MODE_DECLARATIONS['M_h']
        body = Body(('m1', 'm8'), ['q(V0)', 'v(V1)'], vtype_dict={'V0': 't1', 'V1': 't1'})
        heads = generate_single_heads([modes[2]], body, Test.type_manager, False)
        contents = set([(item.head.get_content()[0], tuple(item.extra_bodies)) for item in heads])
        self.assertEqual(contents, {('test(V0,V0,C0)', ('t1(C0)',)), ('test(V0,V1,C0)', ('t1(C0)',)),
                                    ('test(V1,V0,C0)', ('t1(C0)',)), ('test(V1,V1,C0)', ('t1(C0)',))})

    def test_generate_single_heads_single_var(self):
        modes = Test.MODE_DECLARATIONS['M_h']
        body = Body(('m1', 'm8'), ['q(V0)', 'v(V0)'], vtype_dict={'V0': 't1'})
        heads = generate_single_heads([modes[2]], body, Test.type_manager, False)
        self.assertEqual(heads[0].head.get_content(), ['test(V0,V0,C0)'])
        self.assertEqual(heads[0].extra_bodies, ['t1(C0)'])
        self.assertEqual(len(heads), 1)

if __name__ == "__main__":
    unittest.main()