'''
Created on Sep 16, 2017

@author: britta
'''
import unittest

from searchspace.rules import CandidateRulePart
from searchspace.varstructures import get_abstracted_items
from searchspace.varstructures import CombiTree
from utils_for_tests import get_mode_declarations_from_file
from utils_for_tests import get_type_manager_from_config


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.type_manager = get_type_manager_from_config()
        cls.MODE_DECLARATIONS = get_mode_declarations_from_file()

    def test_get_abstracted_vars_unordered(self):
        self.assertEqual(get_abstracted_items(['V4', 'V2', 'V0']), ['V0', 'V1', 'V2'])

    def test_get_abstracted_vars_unordered_duplicates(self):
        self.assertEqual(get_abstracted_items(['V4', 'V2', 'V0', 'V2']), ['V0', 'V1', 'V2', 'V1'])

    def test_get_abstracted_vars_unordered_multiple_prefixes(self):
        self.assertEqual(get_abstracted_items(['V4', 'T2', 'V0', 'T2']), ['V0', 'T1', 'V2', 'T1'])

    def test_combi_tree(self):
        ct = CombiTree(3, 'V')
        var_combis = ct.get_var_arrangements()
        tuple_var_combis = set([tuple(item) for item in var_combis])
        result = {('V0', 'V1', 'V2'),
                  ('V0', 'V0', 'V0'),
                  ('V0', 'V0', 'V1'),
                  ('V0', 'V1', 'V0'),
                  ('V0', 'V1', 'V1')}
        self.assertEqual(tuple_var_combis, result)

    def test_combi_tree_multiple_twice(self):
        """
        careful with number of possibilities for twice two same variables, its the same to first choose position 0 and 1 for one
        duplicate variable and then position 2 and 3 for the second duplicate variable as doing it the other way around, i.e.
        the number of possibilities must be divided by two in that case.
        """
        ct = CombiTree(5, 'V')
        var_combis = ct.get_var_arrangements()
        combis = [['V0', 'V0', 'V1', 'V1', 'V2'], ['V0', 'V1', 'V0', 'V1', 'V2'], ['V0', 'V1', 'V1', 'V0', 'V2']]

        self.assertTrue(all(x in var_combis for x in combis))
        self.assertTrue(len(var_combis) == 52)

    def test_combi_tree_with_dict(self):
        reps = {2: 1, 3: 0}
        ct = CombiTree(4, 'V', repetition_dict=reps)
        var_combis = ct.get_var_arrangements()
        invalid_combis = [['V0', 'V0', 'V1', 'V1'], ['V0', 'V0', 'V0', 'V1']]

        self.assertTrue(all(x not in var_combis for x in invalid_combis))
        self.assertTrue(len(var_combis) == 8)

    def test_combi_tree_with_maxv(self):
        ct = CombiTree(4, 'V', maxv=2)
        var_combis = ct.get_var_arrangements()
        invalid_combis = [['V0', 'V0', 'V1', 'V2'], ['V0', 'V1', 'V0', 'V2'], ['V0', 'V1', 'V2', 'V3']]

        self.assertTrue(all(x not in var_combis for x in invalid_combis))
        self.assertTrue(len(var_combis) == 8)

    def test_combi_tree_with_maxv_and_dict(self):
        reps = {2: 1, 3: 0}
        ct = CombiTree(4, 'V', repetition_dict=reps, maxv=3)
        var_combis = ct.get_var_arrangements()
        invalid_combis = [['V0', 'V0', 'V1', 'V1'], ['V0', 'V0', 'V0', 'V1'], ['V0', 'V1', 'V2', 'V3']]

        self.assertTrue(all(x not in var_combis for x in invalid_combis))
        self.assertTrue(len(var_combis) == 7)

    def test_postprocessing(self):
        # a(_,_),b(_),a(_,_)
        modes = Test.MODE_DECLARATIONS['M_b']
        body_candidate = CandidateRulePart([modes[10], modes[1], modes[10]], Test.type_manager)
        valid_combis = CombiTree.postprocess_var_arrangements([['V0', 'V1', 'V2', 'V3', 'V0'],
                                                               ['V0', 'V1', 'V2', 'V1', 'V3'],
                                                               ['V0', 'V0', 'V1', 'V1', 'V1']],
                                                              body_candidate)
        self.assertEqual(valid_combis, [['V0', 'V1', 'V2', 'V3', 'V0'], ['V0', 'V0', 'V1', 'V1', 'V1']])


if __name__ == "__main__":
    unittest.main()
