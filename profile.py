"""profile the ILASP implementation

Usage:
  profile.py run <problem_def_file>... [--outdir=<dir>]
  profile.py inspect <profile_file>
  profile.py (-h | --help)
  profile.py --version

Options:
  --outdir=<dir>  Output dir to place the results in  [default: bench]

"""
from __future__ import print_function
from docopt import docopt
import re
import subprocess
import os.path
import ConfigParser
import json
import shutil
from collections import defaultdict, namedtuple


def run_py_ilasp(problem_def_file, algorithm, out_base_dir):
    # get the name from the config
    name = retrieve_name(problem_def_file)
    out_dir = os.path.join(out_base_dir, name, algorithm)
    # clear output dir
    shutil.rmtree(out_dir, True)
    subprocess.check_call([
        'python2', 'ilasp.py',
        'solve',
        problem_def_file,
        '--algorithm={}'.format(algorithm),
        '--logdir={}'.format(out_dir),
    ])
    return name, algorithm, os.path.join(out_dir, 'profiling.json')


def retrieve_name(config_path):
    config = ConfigParser.SafeConfigParser()
    config.read(config_path)
    return config.get('ProblemInstance', 'name')


def post_process(name, algorithm, path):
    with open(path) as fd:
        data = json.load(fd)
    print('------------------------------------------------------')
    print("name:", name, "algorithm:", algorithm)
    print("total time:", data['duration'], sep='\t')
    inspect(data)


def inspect(data):
    fix_names(data)

    print("------------------------------------------------------")
    print("name hierarchie (total usages, total duration):")
    hierarchie = create_name_hierarchie(data)
    squashed = squash_name_hierarchie(hierarchie)
    print_squashed_name_hierarchie(squashed)

    print("\n")
    data = first_solution_time(data)
    if data is None:
        print("profile data is from a execution not findinf any solution")
    else:
        time, child, total_step_duration_until_last_step = data
        print("time to find _any_ solution in last iteration: {:.2f}".format(time))
        print("total step duration until last step: {:.2f}".format(total_step_duration_until_last_step))


NAME_PARTS = re.compile("^(.+?)(?:\[([0-9]+)\])?$")


def first_solution_time(data):
    ilasp_run, = children_by_name(data, "ILASP.run_ilasp")
    algorithm = ilasp_run['metadata'][0]
    steps = list(children_by_name(ilasp_run, "ILASP.iteration_step"))
    last_step = max(steps, key=lambda step: step['name'][1])
    step_duration_until_last_step = sum(step['duration'] for step in steps if step['name'][1] < last_step['name'][1])
    if algorithm == 'iter_pos_validate':
        pphase, = children_by_name(last_step, "Control.run_positive_phase")
        time_acc = 0
        for child in pphase['children']:
            if child['name'][0] != "Control.run_positive_phase.next_solution":
                continue
            time_acc += child['duration']
            #TODO currently raises an error here but the problem is in the profiling data
            # we only should have _1_ validation per next_solution, but we have many???
            # potentially some parten issues??
            validate, = children_by_name(child, "ILASP.validate_solution")
            if 'is_solution' in validate.get('metadata', []):
                return time_acc, child, step_duration_until_last_step
        return None
    elif algorithm == 'iter_neg_pos':
        pphase, = children_by_name(last_step, "Control.run_positive_phase")
        neg_phase, = children_by_name(last_step, "Control.run_negative_phase")
        sol_steps = list(children_by_name(pphase, "Control.run_positive_phase.next_solution"))
        return neg_phase['duration'] + sol_steps[0]['duration'], sol_steps[0], step_duration_until_last_step
    else:
        raise ValueError('unknow algorithm', algorithm)


def children_by_name(data, name):
    return (child for child in data['children'] if child['name'][0] == name)


def fix_names(data):
    _name = data['name']
    name, _idx = NAME_PARTS.findall(_name)[0]
    if _idx:
        idx = int(_idx)
    else:
        idx = 0

    data['name'] = (name, idx)

    for child in data['children']:
        fix_names(child)


def create_name_hierarchie(data):
    (name, idx) = data['name']
    duration = data['duration']

    children = {}
    for child in data['children']:
        children.update(create_name_hierarchie(child))

    return {(name, idx): (duration, children)}


def squash_name_hierarchie(hierarchie):
    new = defaultdict(lambda: [1, 0, dict()])
    for (key, _count_id), (duration, sub) in hierarchie.items():
        if key in new:
            new[key][0] += 1
            new[key][1] += duration
            _merge_into(new[key][2], squash_name_hierarchie(sub))
        else:
            new[key] = [1, duration, squash_name_hierarchie(sub)]
    return new


def _merge_into(new, hierachie_b):
    for key, (count, duration, sub) in hierachie_b.items():
        count = count if count else 1
        if key in new:
            new[key][0] += count
            new[key][1] += duration
            _merge_into(new[key][2], sub)
        else:
            new[key] = [count, duration, sub]


def print_squashed_name_hierarchie(hierarchie, indent=0):
    for name, (count, duration, sub) in hierarchie.items():
        # some "marker" use __ as prefix, but they do not make any sense once thinks got squashed
        # so we do not print them here
        if not name.startswith('__'):
            print("  " * indent, "{} [{}x, {:.2f}s]".format(name, count, duration))
            print_squashed_name_hierarchie(sub, indent + 1)


def main():
    arguments = docopt(__doc__, version="profile.py v0.1")
    prob_files = arguments['<problem_def_file>']
    base_dir = arguments['--outdir']

    if arguments['run']:

        # run stuff
        profile_files = []
        for prob_file in prob_files:
            for algorithm in ['iter_neg_pos', 'iter_pos_validate']:
                profile_files.append(run_py_ilasp(prob_file, algorithm, base_dir))

        for name, algorithm, path in profile_files:
            post_process(name, algorithm, path)

    elif arguments['inspect']:
        prof_file = arguments['<profile_file>']
        with open(prof_file) as f:
            data = json.load(f)
        inspect(data)

if __name__ == '__main__':
    main()