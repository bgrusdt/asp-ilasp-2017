from six.moves import zip
from collections import defaultdict
from collections import namedtuple
import itertools
from scipy.special import comb

from asp_representation.modes import ModeDeclaration
import rules
import tricks
import utils
from utils.logger import IlaspLogger
from utils.rules_utils import PredicateAccessor

logger = IlaspLogger.getLogger(__name__)
MetaAgg = namedtuple('MetaAgg', 'bounds')
SingleHead = namedtuple('SingleHead', ['head', 'extra_bodies'])
Replacement = namedtuple('Replacement', ['indices', 'new_var'])


def build_aggregate_bounds(max_rule_length, minh=1, maxh=float('inf')):
    """
    :return dictionary from aggregate length to dictionary from #head-literals to list of
    MetaAgg tuples
    """
    nb_head_literals = minh
    aggregate_bounds = defaultdict(dict)
    while nb_head_literals ** 2 <= max_rule_length and nb_head_literals <= maxh:
        for l in range(0, nb_head_literals + 1):
            for k in range(l, nb_head_literals + 1):
                # useless head aggregates if upper and lower bound 0 and if upper and lower
                # bound equals the number of predicates inside the aggregate
                if((l == 0 and k == 0) or (l == nb_head_literals and k == l)):
                    continue
                agg_length = get_aggregate_length_by_nbs(nb_head_literals, (l, k))
                if agg_length <= max_rule_length:
                    if nb_head_literals not in aggregate_bounds[agg_length].keys():
                        aggregate_bounds[agg_length][nb_head_literals] = []
                    aggregate_bounds[agg_length][nb_head_literals].append(MetaAgg(bounds=(l, k)))
                else:
                    continue
        nb_head_literals += 1

    logger.debug('agg-bounds:')
    for agg_length, dictionary in aggregate_bounds.iteritems():
        logger.debug('aggregates length: %s', agg_length)
        for key, val in dictionary.iteritems():
            logger.debug('#head-literals : bounds: %s - %s', key, val)

    return aggregate_bounds


def build_head_var_combis(unique_body_vars, sym_mapping, nb_vars_per_head, multiple_head_vars_disallowed):
    """
    :return list of tuples of str
    """
    nb_head_literals = len(nb_vars_per_head)
    var_combinations = []
    if multiple_head_vars_disallowed:
        for body_var in unique_body_vars:
            head_var_combis = []
            for idx_hid in range(nb_head_literals):
                vars_current_head = ()
                for _ in range(nb_vars_per_head[idx_hid]):
                    vars_current_head += (body_var,)
                head_var_combis.append(vars_current_head)

            var_combinations.append(tuple(utils.flattened_iter(head_var_combis)))
    else:
        head_var_combis = []
        for i in range(nb_head_literals):
            head_var_combis.append(list(itertools.product(unique_body_vars, repeat=nb_vars_per_head[i])))
        var_combinations = list(itertools.product(*head_var_combis))
        var_combinations = [tuple(utils.flattened_iter(queried_list)) for queried_list in var_combinations]

    if sym_mapping:
        forbidden_var_combis = []
        updated_var_combis = []
        for combi in var_combinations:
            equal_combi = list(combi)
            if combi not in forbidden_var_combis:
                updated_var_combis.append(combi)
                replacements = []
                for var, replaced in sym_mapping.iteritems():
                    var_indices_dict = utils.get_all_indices_by_value(combi, [var])
                    if var_indices_dict:
                        var_indices = var_indices_dict.values()[0]
                        replacements.append(Replacement(indices=var_indices, new_var=replaced))

                for replacement in replacements:
                    equal_combi = utils.put_list_elems_by_tuple(equal_combi, replacement.indices, replacement.new_var)

                forbidden_var_combis.append(tuple(equal_combi))
            else:
                continue
        var_combinations = updated_var_combis

    return var_combinations


def compute_swapped_identical_heads(body_redundant_vars, head_var_combi):
    """
    :param list of 2 tuples (maximal len 2) body_redundant_vars:
    """
    mappings = list(zip(body_redundant_vars[0], body_redundant_vars[1]))
    all_vars = list(utils.flattened_iter(mappings))
    var_indices_dict = utils.get_all_indices_by_value(head_var_combi, all_vars)
    updated_combi = list(head_var_combi)

    for old_var, new_var in mappings:
        updated_combi = utils.put_list_elems_by_tuple(updated_combi, var_indices_dict[old_var], new_var)
        updated_combi = utils.put_list_elems_by_tuple(updated_combi, var_indices_dict[new_var], old_var)

    return tuple(updated_combi)


def convert_head_to_meta(head=None):
    if head is None:
        meta_head_string = ''
    elif head.is_aggregate():
        meta_heads = ['e(' + h.rstrip() + ', X)' for h in head.get_content()]
        meta_head_string = str(head.get_aggregate_bounds()[0]) + '{' + ';'.join(meta_heads) + '}' + \
            str(head.get_aggregate_bounds()[1])
    else:
        meta_head_string = 'e(' + head.get_head_str().rstrip() + ', X) '
    return meta_head_string


def generate_valid_aggregate_heads(list_heads_fully_replaced, bounds, body_str_preds=[]):
    aggregate_heads = []
    identical_heads = []
    for replaced_head_strings in list_heads_fully_replaced:
        logger.debug('fully replaced head: %s', replaced_head_strings)
        # Identical predicates in body and head ?
        if any([elem in body_str_preds for elem in replaced_head_strings]):
            logger.debug('rejected: identical preds in body and head aggregate.')
            continue
        # Identical predicates inside head aggregate ?
        if len(set(replaced_head_strings)) != len(replaced_head_strings):
            logger.debug('rejected: identical preds inside head aggregate.')
            continue
        else:
            if set(replaced_head_strings) not in identical_heads:
                identical_heads.append(set(replaced_head_strings))
                for agg in bounds:
                    logger.debug('accepted.')
                    l, k = agg.bounds
                    new_head = Head(replaced_head_strings[:], aggregate=True, agg_bounds=(l, k))
                    aggregate_heads.append(new_head)
            else:
                logger.debug('rejected. TODO')

    return aggregate_heads


def generate_aggregate_heads_no_vars(replaced_heads, bounds):
    aggregate_heads = []
    for agg in bounds:
        l, k = agg.bounds
        for heads in replaced_heads:
            new_head = Head(list(heads), aggregate=True, agg_bounds=(l, k))
            aggregate_heads.append(new_head)
    return aggregate_heads


def generate_aggregate_heads_with_vars(candidate_head, bounds, multiple_head_vars_disallowed, body):
    heads_with_marker = candidate_head.get_original_content()
    nb_vars_per_head = candidate_head.get_nb_vars_per_pred()
    head_vtypes = candidate_head.get_vtypes()
    forbidden_heads = set()

    body_vtype_dict = body.get_vtype_dict()
    unique_body_vars = body_vtype_dict.keys()
    body_str_preds = body.get_replaced_preds()
    redundant_dups = body.get_redundant_dups()
    sym_mapping = body.get_symetric_var_mapping()
    var_combinations = build_head_var_combis(unique_body_vars, sym_mapping, nb_vars_per_head, multiple_head_vars_disallowed)
    # TODO: This doesnt cover every possible case , for now just made sure that one redundant dups predicate with one predicate
    # works, e.g. q(V0),q(V1)
    if redundant_dups:
        logger.debug('body for special agg head: %s', body_str_preds)
        body_vars = body.get_vars_str()
        lists_redundant_vars = []
        for redundants in redundant_dups:
            lists_redundant_vars += utils.get_list_elems_by_tuple(body_vars, redundants)
    else:
        logger.debug('body for normal agg head: %s', body_str_preds)
        lists_redundant_vars = []

    logger.debug('var combis: %s', var_combinations)
    aggregate_heads = []
    for head_var_combi in var_combinations:
        # check if the types correspond to the types used in the body
        logger.debug('current combi: %s', head_var_combi)
        reject, head_vtype_dict = tricks.check_types(head_var_combi, head_vtypes)
        if reject or not set(head_vtype_dict.items()).issubset(set(body_vtype_dict.items())):
            logger.debug('rejected 1.')
            continue
        else:
            if any(map(len, lists_redundant_vars)) > 0:
                if head_var_combi in forbidden_heads:
                    continue
                else:
                    reject = update_forbidden_heads(forbidden_heads, head_var_combi, lists_redundant_vars)
                    if reject:
                        continue

            heads_vars_replaced = utils.ilasp_utils.replace_pattern_in_predicates(heads_with_marker[:], head_var_combi,
                                                                                  ModeDeclaration.var_pattern)
            heads_fully_replaced = ModeDeclaration.replace_constants_in_heads(heads_vars_replaced, candidate_head)

            aggregate_heads += generate_valid_aggregate_heads(heads_fully_replaced, bounds, body_str_preds)

    logger.debug('Leaving generating aggregate heads with vars method ...')
    return aggregate_heads


def generate_aggregate_heads(aggregate_modes, type_manager, bounds, multiple_head_vars_disallowed, body):
    aggregate_heads = []
    candidate_head = rules.CandidateRulePart(aggregate_modes, type_manager)
    heads_with_marker = candidate_head.get_original_content()
    if candidate_head.get_nb_vars() == 0:
        replaced_heads = ModeDeclaration.replace_constants_in_heads(heads_with_marker[:], candidate_head)
        aggregate_heads += generate_valid_aggregate_heads(replaced_heads[:], bounds)
    elif body is None:
        # variables in head aggregate but empty body
        pass
    else:
        new_heads = generate_aggregate_heads_with_vars(candidate_head, bounds, multiple_head_vars_disallowed, body)
        aggregate_heads += new_heads

    return aggregate_heads


def generate_single_heads(head_single_modes, body, type_manager, multiple_head_vars_disallowed):
    """ creates all possible single head predicates for given body.
    :param ModeDeclaration head
    :param Body body: rule body
    :return list of tuple with created Head object in first argument and list of str extra_bodies in second argument
    """
    new_heads = []
    if body is None:
        unique_body_vars = []
        nbBodyVars = 0
        body_preds = []
        body_vtype_dict = {}
        body_redundants = []
    else:
        unique_body_vars = list(set(list(itertools.chain(*body.get_vars_str()))))
        nbBodyVars = len(unique_body_vars)
        body_preds = body.get_replaced_preds()
        body_vtype_dict = body.get_vtype_dict()
        body_redundants = body.get_redundant_dups()
        logger.debug('body redundants: %s.', body_redundants)
        flat_body_redundants = utils.flattened_iter(body_redundants)
        body_redundant_vars = utils.flattened_iter(utils.get_list_elems_by_tuple(body.get_vars_str(), flat_body_redundants))

    for modeh in head_single_modes:
        candidate_head = rules.CandidateRulePart([modeh], type_manager)
        head_consts_replaced = ModeDeclaration.replace_constants_in_heads([modeh.get_original_content()], candidate_head)

        for head_replaced in head_consts_replaced:
            head_string = head_replaced[0]
            if modeh.get_nb_vars() == 0:
                if not body_redundants:
                    # body_preds is always positive, ok since we want to avoid p :- not p. (contradiction)
                    if head_string not in body_preds:
                        new_head = Head([head_string])
                        new_heads.append(new_head)
            else:
                head_vtypes = modeh.get_vtypes()
                if multiple_head_vars_disallowed:
                    idx_combinations = list(itertools.combinations(range(0, nbBodyVars), 1))
                else:
                    idx_combinations = list(itertools.product(range(0, nbBodyVars), repeat=modeh.get_nb_vars()))

                for indices in idx_combinations:
                    head_var_combi = utils.get_list_elems_by_tuple(unique_body_vars, indices)
                    reject, head_vtype_dict = tricks.check_types(head_var_combi, head_vtypes)
                    if not reject and set(head_vtype_dict.items()).issubset(set(body_vtype_dict.items())):
                        list_head_strings = utils.ilasp_utils.replace_pattern_in_predicates([head_string], head_var_combi,
                                                                                            ModeDeclaration.var_pattern)
                        for current_head in list_head_strings:
                            if current_head not in body_preds:
                                new_head = Head([current_head])
                                if body_redundants and all([body_var in new_head.get_vars() for body_var in body_redundant_vars]) or not body_redundants:
                                    new_heads.append(new_head)

    return new_heads


def get_aggregate_length_by_agg(aggregate):
    """
    :param aggregate: string, e.g. 0{a;b}1
    ;return int: nb of literals in DNF of aggregate corresponding to the length of the aggregate
    """
    aggregate = aggregate.strip()
    min_val = int(aggregate[0])
    max_val = int(aggregate[-1])

    # TODO: ; can be used elsewhere except for the separation of head literals inside an aggregate?
    content = aggregate[1:-1]
    nb_aggregate_parts = len(content.split(';'))

    nb_literals = 0
    for k in range(min_val, max_val + 1):
        nb_literals += comb(nb_aggregate_parts, k, exact=True)

    return nb_literals * nb_aggregate_parts


def get_aggregate_length_by_nbs(nb_head_preds, bounds):
        """
        :param nb_head_preds: nb of predicates inside aggregate
        :param bounds: tuple containing the lower and the upper bound as ints
        :return nb of literals in DNF of aggregate corresponding to the length of the aggregate
        :rtype int
        """
        agg_length = 0
        for k in range(bounds[0], bounds[1] + 1):
            agg_length += comb(nb_head_preds, k, exact=True)

        return agg_length * nb_head_preds


def update_forbidden_heads(set_forbidden_heads, head_var_combi, lists_redundant_vars):
    reject = False
    redundant_vars = list(utils.flattened_iter(lists_redundant_vars))
    nb_red_vars = len(redundant_vars)
    if len(head_var_combi) > 1:
        identical_combi = tuple(compute_swapped_identical_heads(lists_redundant_vars, head_var_combi))
        set_forbidden_heads.add(identical_combi)
    if nb_red_vars <= len(head_var_combi):
        vars_to_indices = utils.get_all_indices_by_value(head_var_combi, redundant_vars)
        # all redundant variables have to be in the head
        if set(vars_to_indices.keys()) == set(redundant_vars):
            identicals = list(itertools.permutations(head_var_combi, nb_red_vars))
            for combi in identicals:
                indices = utils.flattened_iter(vars_to_indices.values())
                new_combi = tuple(utils.put_list_elems_by_tuple(list(head_var_combi), indices, combi))
                if new_combi != head_var_combi:
                    set_forbidden_heads.add(new_combi)
        else:
            logger.debug('rejected: not all redundant variables inside the head.')
            reject = True
    elif all([var in redundant_vars for var in head_var_combi]):
        identicals = list(itertools.permutations(redundant_vars, len(head_var_combi)))
        for combi in identicals:
            if combi != head_var_combi:
                set_forbidden_heads.add(combi)

    return reject


class Head:

    def __init__(self, list_preds, aggregate=False, agg_bounds=None):
        """
        :param list of str list_preds: valid asp-head strings
        :param bool aggregate
        :param tuple agg_bounds lower bound, upper bound
        """
        preds = [''.join(pred.split()) for pred in list_preds]
        self._list_content = preds

        predAcc = PredicateAccessor(self.get_content())
        self._list_str_vars = predAcc.get_vars_str()

        self._is_aggregate = aggregate
        self._agg_bounds = agg_bounds

    def get_aggregate_bounds(self):
        """
        :return 2-ary tuple with lower and upper aggregate bound or None if it is not an aggregate
        """
        return self._agg_bounds

    def get_content(self):
        return list(self._list_content)

    def get_head_str(self):
        if self._is_aggregate:
            head_str = str(self._agg_bounds[0]) + '{' + ';'.join(self._list_content) + '}' + str(self._agg_bounds[1])
        else:
            head_str = ''.join(self._list_content)
        return head_str

    def get_vars(self):
        return list(self._list_str_vars)

    def is_aggregate(self):
        return self._is_aggregate
