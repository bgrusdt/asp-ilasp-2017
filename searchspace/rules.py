import itertools
from collections import defaultdict

import bodies
import heads
import utils
import varstructures
from utils import index_multiple
from utils.logger import IlaspLogger

logger = IlaspLogger.getLogger(__name__)


def convert_rule_to_meta(rule_id, body=None, head=None, extra_bodies=None):
    """builds rule in meta format as described in ILASP paper from body_elems and head and if applicable adds extra_bodies as
    they are to the body and puts 'not' before the predicates at indices_negated_preds.
    :param Body body
    :param Head head
    :param list of str extra_bodies: list of str with body predicates that will not be converted to meta format
    :return 1. (str) corresponding meta rule
            2. (str) rule id
    """
    if not head and not body:
        message = 'rule creation requested with no head and no body'
        logger.error(message)
        raise ValueError(message)

    meta_head_string = heads.convert_head_to_meta(head)
    meta_body_string = bodies.convert_body_to_meta(rule_id, body, extra_bodies)

    return meta_head_string + ' :- ' + meta_body_string + '.'


def generate_rule_str(body=None, head=None, extra_bodies=None):

    no_head = False
    if head is None:
        head_str = ''
        no_head = True
    else:
        head_str = head.get_head_str()

    if body is None:
        body_str = ''
        if no_head:
            message = 'Rules without body and head cannot be generated.'
            logger.warn(message)
    else:
        neg_indices = body.get_indices_negated_preds()
        list_preds = body.get_replaced_preds()
        for idx in neg_indices:
            list_preds[idx] = 'not ' + list_preds[idx]
        body_str = ','.join(list_preds)

    if extra_bodies:
        body_str = body_str + ','.join(extra_bodies)

    return head_str + ':-' + body_str + '.'


class CandidateRulePart:

    def __init__(self, modes, type_manager):

        self._init_default_values()
        constant_combis = []
        for idx, mode in enumerate(modes):
            mid = mode.get_mode_id()
            self._combi.append(mid)
            self._nb_vars_per_pred.append(mode.get_nb_vars())
            self._nb_consts_per_pred.append(mode.get_nb_consts())
            self._orig_content.append(''.join(mode.get_original_content().split()))
            if mode.is_symmetric():
                self._symetric_ids.append(mid)
            if mode.is_anti_reflexive():
                self._anti_refs.append(idx)
            if mode.is_positive():
                self._positives.append(idx)
            if mode.is_head():
                constant_combis.append(mode.get_const_patterns(type_manager))
            self._vtypes += mode.get_vtypes()

        self._nb_vars = sum(self._nb_vars_per_pred)
        if constant_combis:
            non_const_indices = []
            const_indices = []
            for idx, sublist in enumerate(constant_combis):
                if len(sublist) == 0:
                    non_const_indices.append(idx)
                else:
                    const_indices.append(idx)
            if len(non_const_indices) > 0:
                self._constant_combis = list(itertools.product(*index_multiple(constant_combis, const_indices)))
            else:
                self._constant_combis = list(itertools.product(*constant_combis))

        self._type_manager = type_manager

    def _init_default_values(self):
        self._abstraction = None
        self._var_indices = None
        self._anti_refs = []
        self._combi = []
        self._constant_combis = []
        self._const_indices = None
        self._duplicates = None
        self._nb_consts_per_pred = []
        self._nb_vars_per_pred = []
        self._orig_content = []
        self._positives = []
        self._symetric_ids = []
        self._vtypes = []

    def _compute_indices(self, ttype):
        if ttype.lower() == 'v':
            self._var_indices = []
            indices = self._var_indices
            items_per_pred = self._nb_vars_per_pred
        elif ttype.lower() == 'c':
            self._const_indices = []
            indices = self._const_indices
            items_per_pred = self._nb_consts_per_pred
        start = 0
        for nb_items in items_per_pred:
            indices.append(range(start, start + nb_items))
            start += nb_items

    def get_abstraction(self):
        if self._abstraction is None:
            abstract_combi = varstructures.get_abstracted_items(self._combi)
            self._abstraction = tuple([(self._nb_vars_per_pred[idx], abstract_combi[idx]) for idx in range(len(abstract_combi))])

        return self._abstraction

    def get_anti_ref_indices(self):
        return list(self._anti_refs)

    def get_combi(self):
        return tuple(self._combi)

    def get_constant_combis(self):
        return self._constant_combis

    def get_const_indices(self, pred_indices):
        if self._const_indices is None:
            self._compute_indices('c')
        return utils.get_list_elems_by_tuple(self._const_indices, pred_indices)

    def get_duplicates_dict(self):
        # TODO: is not a deep copy
        if self._duplicates is None:
            self._duplicates = utils.get_duplicates(self._combi)
        return self._duplicates

    def get_duplicate_pred_ids(self):
        if self._duplicates is None:
            self._duplicates = utils.get_duplicates(self._combi)
        return self._duplicates.keys()

    def get_duplicate_indices(self):
        if self._duplicates is None:
            self._duplicates = utils.get_duplicates(self._combi)
        return self._duplicates.values()

    def get_nb_consts_per_pred(self):
        return list(self._nb_consts_per_pred)

    def get_nb_vars(self):
        return self._nb_vars

    def get_nb_vars_per_pred(self):
        return self._nb_vars_per_pred

    def get_original_content(self):
        return list(self._orig_content)

    def get_positive_indices(self):
        return list(self._positives)

    def get_symetric_ids(self):
        return list(self._symetric_ids)

    def get_type_manager(self):
        return self._type_manager

    def get_var_indices(self, pred_indices):
        if self._var_indices is None:
            self._compute_indices('v')
        return utils.get_list_elems_by_tuple(self._var_indices, pred_indices)

    def get_var_indices_dict(self):
        mid_to_var_indices = defaultdict(list)
        for idx, mid in enumerate(self.get_combi()):
            mid_to_var_indices[mid] += self.get_var_indices([idx])
        return mid_to_var_indices

    def get_vtypes(self):
        return list(self._vtypes)
