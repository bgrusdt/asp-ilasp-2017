import clingo
import constants
import asp_clingo.ast as ast
from asp_representation.background import rewrite_program
from collections import defaultdict
from utils.generators import IDGenerator
from itertools import chain

def load(file, extra_args):
    entries = []
    with open(file, 'r') as f:
        for line in f:
            if line.startswith('#'):
                continue
            else:
                length, asp = line.split('~', 1)
                entries.append((length.strip(), asp.strip()))

    return SearchSpace.from_raw(entries, extra_args)


def wrap_rule(rid, str_asp_rule):
    """
    wraps a (str) asp rule so that it can be used with the ILASP algorithm
    :param str_asp_rule:
    :return str:
    """

    # 1. parse as ast node (works on a _list_ of rules)
    ast_prog = ast.parse_program(str_asp_rule)[1:]
    # 2. rewrite rule to have wrapping (works on a _list_ of rules)
    rewrite_program(ast_prog)
    # 3. get rule (it's just one)
    rule, = ast_prog
    # 4. add the active(rid) predicate
    rule.body.append(ast.ASTLiteral(
        atom=clingo.ast.SymbolicAtom(
            term=ast.ASTFunction(
                name=constants.ASP_NAME_OF_ACTIVE,
                arguments=[ast.ASTVariable(rid)]
            )
        )
    ))
    str_rule = repr(rule)
    return str_rule


class SearchSpace(object):

    @staticmethod
    def from_raw(entries, extra_args):
        """
        Create a pre-generated searchspace from a dict mapping a rule length
        to a list of (string) asp rules.

        :param entries: iterable of length, str_asp tuples
        :return SearchSpace: a new SearchSpace instance
        """

        rid_gen = IDGenerator(constants.RULE_PREFIX)

        with_rids = ((rid_gen.nextId(), data) for data in entries)

        entries = (
            (int(length), rid, str_rule, wrap_rule(rid, str_rule))
            for rid, (length, str_rule) in with_rids
        )

        return SearchSpace(entries, extra_args)

    def __init__(self, entries, extra_args):
        self._length2rule_mapping = defaultdict(list)
        self._length2length_rule_mapping = defaultdict(list)
        self._id2rule_mapping = {}
        for (length, rid, str_rule, wrapped_rule) in entries:
            self._length2rule_mapping[length].append((rid, str_rule, wrapped_rule))
            self._id2rule_mapping[rid] = str_rule
            self._length2length_rule_mapping[length].append("length({},{}).".format(rid, length))
        self.max_rule_length = extra_args['max_rule_length']

    def build_search_space(self, rule_length):
        if rule_length > self.max_rule_length:
            return [], []

        generated_rules = []
        rule_ids = []
        for (rid, _rule, wrapped_rule) in self._length2rule_mapping.get(rule_length, []):
            rule_ids.append(rid)
            generated_rules.append(wrapped_rule)

        generated_rules.extend(self._length2length_rule_mapping.get(rule_length, []))

        return rule_ids, generated_rules

    def get_rules_by_length(self, length):
        """
        :return List[str]: list of string representations of rules
        """
        return list(chain(
            (wrapped for (rid, _rule, wrapped) in self._length2rule_mapping.get(length, [])),
            self._length2length_rule_mapping.get(length, [])
         ))

    def get_rule_by_id(self, rid):
        """
        :return str: string representation of the rules
        """
        rule = self._id2rule_mapping.get(rid, None)
        if rule is None:
            raise ValueError("The rule with id {} does not exist.".format(rid))
        return rule

    @staticmethod
    def get_rid_prefix():
        return constants.RULE_PREFIX