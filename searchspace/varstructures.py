import copy
import itertools
import numpy as np
import re
from collections import defaultdict
from collections import namedtuple

from utils.generators import IDGenerator
# from utils.logger import IlaspLogger

# logger = IlaspLogger.getLogger(__name__)
Combi = namedtuple('Combi', ['taken', 'free'])


def get_abstracted_items(var_list):
    """makes abstraction from body variables to specific variable representation: first occurence of a variable is called
    prefix+0, second occurence prefix+1 and so on.
    :param list of str var_list: representing variables of one rule/body/head
    """
    var_count = 0
    mapping = {}
    regex = r"(\b[A-Za-z]+)"
    for current_var in var_list:
        if(current_var not in mapping.keys()):
            if len(current_var.split()) != 1:
                raise ValueError('Variables must not contain whitespaces.')
            try:
                var_prefix = re.search(regex, current_var).group()
            except Exception, e:
                raise ValueError('Variables must start with a letter.' + str(e))

            mapping[current_var] = var_prefix + str(var_count)
            var_count += 1

    var_string = ";".join(var_list)
    # make temporary variables
    for old_var in mapping.keys():
        var_string = var_string.replace(old_var, "$" + old_var + "$")
    # replace temporary by new variables
    for old_var in mapping.keys():
        var_string = var_string.replace("$" + old_var + "$", mapping[old_var])

    abstracted_var_list = var_string.split(";")

    return abstracted_var_list


class Node:

    def __init__(self, value, level, vdict, nodeId):

        self.value = value
        self.level = level
        self.vdict = vdict

        self.id = nodeId


class CombiTree:

    def __init__(self, nbVars, var_prefix, repetition_dict={}, maxv=None):
        """
        :param int nbVars: number of variable slots
        :param str _var_prefix: variables will have the form '_var_prefix'+int, e.g. V0,V1 etc. for _var_prefix = 'V'
        :param dictionary repetition_dict: from ntimes a variable can be taken, e.g. repetition_dict = {2:2, 3:1} and
        nbVars=6 means that at most two variables can be taken twice and that at most 1 variable can be taken three times
        For the values that are not specified in the dictionary, in this case 4,5 and 6, all possibilities are generated as if
        no dictionary was specified. Impossible values, such as {3: 2} for nbVars=4 are just ignored and all possible
        combinations are generated for that level (here 3).
        If repetition_dict is empty (default case), it means that all possible variable combinations will be considered.
        :param int maxv: maximal number of variables that are accepted in one variable combination
        """
        self._maxv = maxv
        if(self._maxv is None):
            self._maxv = nbVars
        self._var_prefix = var_prefix
        self._nb_vars = nbVars
        self.dicts = []
        rootValue = 0
        rootLevel = 1
        rootDict = defaultdict(list)
        self.idGenerator = IDGenerator('N')
        root = Node(rootValue, rootLevel, rootDict, self.idGenerator.nextId())

        self.var_positions = [n for n in range(0, nbVars)]
        self._repetitions = repetition_dict

        var_arrangements = []
        self._buildup_tree([root], var_arrangements)
        self._var_arrangements = var_arrangements

#         logger.debug('%s arrangements for variables found for %s variables before processing.', len(var_arrangements), nbVars)

    def _buildup_tree(self, nodes, var_arrangements):
        """
        :param list of Nodes nodes: Nodes already inside this tree
        :param list _var_arrangements
        """
        for node in nodes:
            # print('node: ', node.id)
            nodes_currentLevel = []
            if node.value <= self._nb_vars:

                nextLevel = node.level + 1
                # build this level
                if(nextLevel <= self._nb_vars):
                    #  val + x * level <= _nb_vars
                    upto = (self._nb_vars - node.value) / nextLevel  # ganzzahlige division!

                    if(upto == 0):
                        # no changes made if next level taken 0 times
                        var_arrangements += self._build_var_arrangements(node.vdict)

                    else:

                        if(nextLevel in self._repetitions):
                            value = self._repetitions[nextLevel]
                            if(value * nextLevel <= self._nb_vars):
                                upto = self._repetitions[nextLevel]

                        for ntimes in range(0, upto + 1):
                            # print('ntimes:', str(ntimes))

                            newVal = node.value + ntimes * nextLevel
                            newDict = dict(node.vdict)
                            newDict[nextLevel] = ntimes
                            nodes_currentLevel.append(Node(newVal, nextLevel, newDict, self.idGenerator.nextId()))

                        self._buildup_tree(nodes_currentLevel, var_arrangements)
                else:

                    var_arrangements += self._build_var_arrangements(node.vdict)

    def _get_indices(self, taken_to_free_dict, nreps, nvars):
        """iterates over all possibilities to take nreps out of the free positions specified as list of int in values in
        taken_to_free_dict. nvars is the number of times how often the process is repeated recursively. Stops if 0.
        :returns a new dictionary from taken positions (extending those that had already been taken before) to the remaining free
        positions which are computed.
        """
        if(nvars == 0):
            return taken_to_free_dict
        else:
            new_dict = {}
            for taken, free in taken_to_free_dict.iteritems():
                pos_equal_vars = list(itertools.combinations(free, nreps))
                for combi in pos_equal_vars:
                    new_key = list(taken)
                    new_key.append(combi)
                    new_dict[tuple(new_key)] = [i for i in free if i not in combi]

            return self._get_indices(new_dict, nreps, nvars-1)

    def _build_var_arrangements(self, vdict):
        """ The returned variable arrangements are already normalized, i.e. first occurence of a variable is called V0, second
        new variable V1 etc. e.g. V0,V1,V0 but not V1,V0,V1.
        :return a list of lists containing all different variable arrangements
        """
        keys = vdict.keys()
        poslist = self.var_positions
        varlist = [self._var_prefix + str(idx) for idx in poslist]

        all_possibilities = []
        taken = ()
        free = poslist
        combi_dict = {taken: free}
        for nreps in keys:
            nvars = vdict[nreps]
            if(nvars == 0):
                continue

            combi_dict = self._get_indices(combi_dict, nreps, nvars)

        if(() not in combi_dict.keys()):
            for taken_combi, free_combi in combi_dict.iteritems():
                if(len(taken_combi) + len(free_combi) > self._maxv):
                    continue
                arrangement = np.asarray(poslist)
                nbvars = 0
                for indices_one_var in taken_combi:
                    var_assignments = [nbvars for _ in range(len(indices_one_var))]
                    arrangement[list(indices_one_var)] = var_assignments
                    nbvars += 1

                for idx in free_combi:
                    arrangement[idx] = nbvars
                    nbvars += 1

                arrangement = arrangement.tolist()
                arrangement = [self._var_prefix + str(item) for item in arrangement]
                all_possibilities.append(arrangement)

        # all variables are different (each value in dictionary is 0)
        if(not all_possibilities):
            if(len(set(varlist)) <= self._maxv):
                all_possibilities.append(varlist)

        # there might be duplicates in the arrangements since if several variables can appear several times e.g.
        # 4 variables at postiions 0 1 2 3 if 2 variables appear twice:
        # 1. we choose 2 positions out of the 4
        # 2. then we choose two of the remaining positions which is deterministic in this case
        # so chooosing 0-3 in the first place and 1-2 in the second results in the same arrangement as
        # 1-2 0-3.
        abstractions = []
        for arrangement in all_possibilities:
            abstracted_arr = get_abstracted_items(arrangement)
            if(abstracted_arr not in abstractions):
                abstractions.append(abstracted_arr)

        return abstractions

    def get_var_arrangements(self):
        return copy.deepcopy(self._var_arrangements)

    @staticmethod
    def postprocess_var_arrangements(list_var_arrangements, body_candidate):
        """If the rule for which this variable arrangement is used has duplicate predicates inside, some arrangements become
        identical, e.g. a(V0,V1) a(V2,V0) == a(V2,V0), a(V0,V1) (standardized: a(V0,V1), a(V1,V2)).
        These identicals are removed here and the valid arrangements are
        returned.
        :param list of list of str var_arrangements
        :param dict mode_id_to_var_indices: identifier of the duplicate mapped to a list of lists each containing the
        variable indices of that mode_id in this body_mode_combi, e.g.
        a(V) a(V) b(V,V) c(V) c(V) --> {m0: [[0],[1]], m1: [[2,3]], m2: [[4],[5]]}
        :param list of str body_mode_combi: containing ids of modes of one specific combination of body modes,
        e.g. [m0, m0, m1, m2, m2]
        :param str vprefix: prefix for variables
        :return list of list of str: valid var arrangements
        """
        duplicates = body_candidate.get_duplicate_pred_ids()
        nb_dup_predicates = len(duplicates)
        if nb_dup_predicates == 0:
            return list_var_arrangements

        var_indices_per_pred = body_candidate.get_var_indices_dict()
        var_indices_per_pred = np.asarray(var_indices_per_pred)
        mid_to_var_indices = body_candidate.get_var_indices_dict()

        valid_arrangements = []
        forbidden_arrangements = []
        nbinforbidden = 0
        for varArrangement in list_var_arrangements:
            if(varArrangement in forbidden_arrangements):
                nbinforbidden += 1
#                 logger.debug('%s', varArrangement)
                continue
            else:
                valid_arrangements.append(varArrangement)
                original_varArrangement = np.asarray(varArrangement)

                all_possibilities = {}
                for pred in duplicates:
                    # build all possible orders of each duplicate predicate; e.g.
                    # a(X,Y),a(Z,W),b(T), b(U) if a is replaced, it could either be 0,1 (X,Y)(Z,W) or 1,0 (Z,W)(X,Y)
                    nb_occ_current_pred = len(mid_to_var_indices[pred])
                    combis = list(itertools.permutations(range(nb_occ_current_pred), nb_occ_current_pred))
                    var_indices = np.asarray(mid_to_var_indices[pred])
                    var_combis = []
                    for combi in combis:
                        new_indices = var_indices[list(combi)].tolist()
                        var_combis.append(new_indices)

                    all_possibilities[pred] = var_combis

                # iterate over the nb of replaced duplicate predicates for which all semantically identical possibilities
                # are found and joined
                nb_replacements = range(1, nb_dup_predicates + 1)
                for nb_replaced in nb_replacements:
                    # get the predicates which will be replaced
                    combis_preds_replaced = list(itertools.permutations(duplicates, nb_replaced))
                    arrangements = []
                    idx_to_pred_name = {}
                    for combi in combis_preds_replaced:
                        for i, pred in enumerate(combi):
                            idx_to_pred_name[i] = pred
                            arrangements.append(all_possibilities[pred])

                        all_dup_variations = list(itertools.product(*arrangements))

                        # replacements
                        for arrangement in all_dup_variations:
                            # arrangement: tuple containing in each entry one possibility for all occurrences of one duplicate
                            # predicate
                            modified_varArrangement = np.asarray(varArrangement)
                            for i, pred_indices in enumerate(arrangement):
                                # list of lists containing variable indices per predicate occurence
                                original_indices = mid_to_var_indices[idx_to_pred_name[i]]
                                for idx_occ, occ_indices in enumerate(original_indices):
                                    modified_varArrangement[occ_indices] = original_varArrangement[pred_indices[idx_occ]]

                                modified_varArrangement = modified_varArrangement.tolist()
                                # make abstraction
                                modified_varArrangement = get_abstracted_items(modified_varArrangement)
                                if(modified_varArrangement != varArrangement):
                                    forbidden_arrangements.append(modified_varArrangement)

#         logger.debug('%s arrangements out of %s were excluded since in forbidden arrangements.', nbinforbidden,
#                      len(list_var_arrangements))

        return valid_arrangements

if __name__ == '__main__':
    ct = CombiTree(3, 'V')
    vas = ct.get_var_arrangements()
