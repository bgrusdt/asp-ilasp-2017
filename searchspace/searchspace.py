"""
Created on Oct 2, 2017

@author: britta
"""
from six.moves import range
import itertools
import json
from collections import defaultdict
from collections import namedtuple

import bodies
import constants
import heads
import rules
import tricks
import utils
from asp_representation.modes import ModeDeclaration
from utils.logger import IlaspLogger
from varstructures import CombiTree
from utils.profile import profile

LOGGER = IlaspLogger.getLogger(__name__)
Rule = namedtuple('Rule', ['str', 'meta_str', 'id'])


class SearchSpace:

    def __init__(self, ruleId_generator, var_id_generator, path_to_mode_declarations, extra_args, type_manager):
        """
        :param IDGenerator ruleID_generator
        :param IDGenerator varId_generator
        :param path_to_mode_declarations
        :param extra_args: dictionary containing meta arguments
        """
        with open(path_to_mode_declarations) as json_data:
            try:
                mode_dict = json.load(json_data)
            except Exception as e:
                message = '''The ModeDeclarations file must follow the structure specified in the Readme File.'''
                raise ValueError(message + '\n' + str(e))

        modes_b = mode_dict['M_b']
        modes_h = mode_dict['M_h']
        modes_ha = mode_dict['M_ha']
        self.var_id_generator = var_id_generator
        self.rule_id_generator = ruleId_generator
        self._type_manager = type_manager
        self.length_to_rules_dict = defaultdict(list)
        self.ruleID_to_rule_dict = {}
        self.varArrangementTrees = defaultdict(CombiTree)
        self._ppCombis = {}
        self._forbidden_rules = defaultdict(set)

        self._setup_extra_constants(extra_args)
        self._instantiate_modes(modes_b, modes_h, modes_ha)

    def _build_length_rules(self, rule_ids, rule_length):
        """ generates length-rules for given length and rules with given ids
        :param list of int rule_ids: int-parts of the rule ids of the newly added rules
        :param int n: length of added new rules
        :return list of Rules
        """
        length_rules = []
        for rule_id in rule_ids:
            length_rule = "length({},{}).".format(rule_id, rule_length)
            length_rules.append(Rule(str=length_rule, meta_str='', id=''))

        if not rule_ids:
            LOGGER.warn('length rules requested for empty list of rule ids.')
        else:
            LOGGER.debug('length(%s..%s). - Rules build.', rule_ids[0], rule_ids[-1])

        return length_rules

    def build_negated_bodies(self, pos_body):
        combis_negated_preds = bodies.build_negated_pred_combis(pos_body)
        neg_bodies = []
        for indices_neg_preds in combis_negated_preds:
            new_body = bodies.Body(pos_body.get_combi(), pos_body.get_replaced_preds(),
                                   positives=pos_body.get_positive_indices(),
                                   symetric_ids=pos_body.get_symetric_ids(),
                                   vtype_dict=pos_body.get_vtype_dict(),
                                   indices_neg_preds=indices_neg_preds)
            neg_bodies.append(new_body)
        return neg_bodies

    @profile(name="SearchSpace.build_search_space")
    def build_search_space(self, rule_length):
        if rule_length > self.max_rule_length:
            return [], []

        offset_rid = self.rule_id_generator.get_current_id_number()

        LOGGER.info('building rules of length %s: ......', rule_length)
        new_rules = self.generate_rules(rule_length)
        self._add_rules_to_searchspace(rule_length, new_rules)

        end_rule_id = self.rule_id_generator.get_current_id_number()
        prefix = self.rule_id_generator.get_prefix()

        if new_rules:
            rids = [ prefix + str(num_id) for num_id in range(offset_rid + 1, end_rule_id + 1) ]
            length_rules = self._build_length_rules(rids, rule_length)
            self._add_rules_to_searchspace(rule_length, length_rules)
            generated_rules = length_rules + new_rules
            LOGGER.info('# generated rules ~ %s: %s + %s length-rules.', rule_length, len(new_rules), len(length_rules))
            return rids, generated_rules
        return [], []

    def buildup_modeha_rules(self, hcombis_ids, bounds, bodies):
        all_rules = []
        for body in bodies:
            for hcombi in hcombis_ids:
                modeha_combi = [self._head_aggregate_modes[hid] for hid in hcombi]
                valid_aggregate_heads = heads.generate_aggregate_heads(modeha_combi, self._type_manager, bounds,
                                                                       self._multiple_head_vars_disallowed, body)
                for head in valid_aggregate_heads:
                    all_rules.append(self.buildup_rule(head, body))
        else:
            # aggreagte fact.
            for hcombi in hcombis_ids:
                modeha_combi = [self._head_aggregate_modes[hid] for hid in hcombi]
                valid_aggregate_heads = heads.generate_aggregate_heads(modeha_combi, self._type_manager, bounds,
                                                                       self._multiple_head_vars_disallowed, body=None)
                for head in valid_aggregate_heads:
                    all_rules.append(self.buildup_rule(head))

        return all_rules

    def buildup_rule(self, head=None, body=None, extra_bodies=None):
        rule_str = rules.generate_rule_str(body=body, head=head, extra_bodies=extra_bodies)
        rule_id = self.rule_id_generator.nextId()
        meta_rule = rules.convert_rule_to_meta(rule_id, body=body, head=head, extra_bodies=extra_bodies)
        # LOGGER.debug('%s: str: %s  meta: %s', rule_id, rule_str, meta_rule)
        return Rule(str=rule_str, meta_str=meta_rule, id=rule_id)

    def generate_aggregate_head_rules(self, n):
        all_rules = []
        if not self._head_aggregate_modes:
            return all_rules
        body_agg_lengths = self._get_agg_body_lengths(n)

        for (len_body, len_agg) in body_agg_lengths:
            bodies = self._get_bodies(len_body)

            nb_head_preds_to_bounds = self._bound_dict[len_agg]
            list_nb_head_preds = nb_head_preds_to_bounds.keys()
            for hp in list_nb_head_preds:
                hcombis_ids = list(itertools.combinations_with_replacement(self._head_aggregate_modes.keys(), hp))
                bounds = nb_head_preds_to_bounds[hp]
                all_rules += self.buildup_modeha_rules(hcombis_ids, bounds, bodies)

        return all_rules

    def generate_constraints(self, n):
        constraints = []
        bodies = self._get_bodies(n)
        for body in bodies:
            if not body.get_redundant_dups():
                constraints.append(self.buildup_rule(body=body))
        return constraints

    def generate_rules(self, rlength):
        generated_rules = []
        generated_rules += self.generate_constraints(rlength)
        generated_rules += self.generate_simple_head_rules(rlength)
        generated_rules += self.generate_aggregate_head_rules(rlength)
        return generated_rules

    def generate_simple_head_rules(self, n):
        simple_head_rules = []
        len_body = n - 1
        if self._head_single_modes:
            if len_body <= self._max_bodies:
                if len_body == 0:
                    bodies = [None]
                else:
                    bodies = self._get_bodies(len_body)
                for body in bodies:
                    valid_heads = heads.generate_single_heads(self._head_single_modes.values(), body, self._type_manager,
                                                              self._multiple_head_vars_disallowed)
                    for head in valid_heads:
                        simple_head_rules.append(self.buildup_rule(head, body))
        return simple_head_rules

    def get_id_prefix(self):
        return self.get_rid_generator().get_prefix()

    def get_rid_generator(self):
        return self.rule_id_generator

    def get_rules_by_length(self, n):
        """
        :return List[str]: list of string representations of rules
        """
        return list(self.length_to_rules_dict[n])

    def get_rule_by_id(self, rid):
        """
        :return str: string representation of the rules
        """
        rule = self.ruleID_to_rule_dict.get(rid, None)
        # do _not_ use "is not None" here
        if not rule:
            raise ValueError("The rule with id {} does not exist.".format(rid))
        return rule

    def _add_rules_to_searchspace(self, hypothesis_length, rules):
        meta_rules = []
        for rule in rules:
            self._saveRule(hypothesis_length, rule)
            if not rule.meta_str:
                LOGGER.debug('~ %s: ' + rule.str, hypothesis_length)
            else:
                LOGGER.debug('~ %s: ' + rule.meta_str, hypothesis_length)
            meta_rules.append(rule.meta_str)

        return meta_rules

    def _check_body_globally(self, body_candidate, bodyvar_type_dict, varArrangement):
        replaced_preds = utils.ilasp_utils.replace_pattern_in_predicates(body_candidate.get_original_content(), varArrangement,
                                                                         ModeDeclaration.var_pattern)
        combi = body_candidate.get_combi()
        body = bodies.Body(combi, replaced_preds, vtype_dict=bodyvar_type_dict, positives=body_candidate.get_positive_indices(),
                           symetric_ids=body_candidate.get_symetric_ids())
        LOGGER.debug('forbidden bodies: %s.', self._forbidden_rules[('', combi)])

        body_str_content_per_pred = body.get_replaced_preds()
        new_body_str = ",".join(body_str_content_per_pred)
        new_bodies = []
        if new_body_str in self._forbidden_rules[('', combi)]:
            return new_bodies

        LOGGER.debug('potential new body: %s for combi: %s.', new_body_str, combi)
        redundant_bodies = tricks.check_body_duplicates(body)
        if redundant_bodies is None:
            keep_body = 0
        elif not redundant_bodies:
            keep_body = 1
        else:
            keep_body = 2
            body.set_redundant_dups(redundant_bodies)
            LOGGER.debug('redundant dups: %s', body.get_redundant_dups())

        LOGGER.debug('body kept: %s.', keep_body)
        if keep_body != 0:
            new_forbidden = tricks.check_global_symmetry(body, body_candidate)
            new_bodies.append(body)
            neg_bodies = self.build_negated_bodies(body)
            if keep_body == 1 and new_body_str not in self._forbidden_rules[('', combi)]:
                # self._forbidden_rules[('', combi)].add(new_body_str)
                if new_forbidden:
                    self._forbidden_rules[('', combi)] = self._forbidden_rules[('', combi)].union(new_forbidden)
            elif keep_body == 2 and new_body_str not in self._forbidden_rules[('agg', combi)]:
                # self._forbidden_rules[('agg', combi)].add(new_body_str)
                if new_forbidden:
                    self._forbidden_rules[('agg', combi)] = self._forbidden_rules[('agg', combi)].union(new_forbidden)
                for neg_body in neg_bodies:
                    neg_body.set_redundant_dups(redundant_bodies)

            new_bodies += neg_bodies

        return new_bodies

    def _generate_bodies(self, n):
        """generates all valid bodies considering anti-reflexive, symmetric, positive, types
        :return list of Body objects
        """
        pred_combis = self._body_pred_combis_per_n[n]
        new_bodies = []
        nb_postprocessed = 0
        for combi in pred_combis:
            modes = utils.get_list_elems_by_tuple(self._body_modes, combi)
            # body_candidate = bodies.BodyCandidate(modes)
            body_candidate = rules.CandidateRulePart(modes, self._type_manager)
            if body_candidate.get_nb_vars() == 0:
                generated_bodies = self._generate_bodies_no_vars(body_candidate)
            else:
                generated_bodies, postprocessed = self._generate_bodies_with_vars(body_candidate)
                if postprocessed:
                    nb_postprocessed += 1

            new_bodies += generated_bodies

        LOGGER.debug('Hypothesis length %s: %s times varArrangements postprocessed out of %s.', n, nb_postprocessed,
                     len(pred_combis))
        return new_bodies

    def _generate_bodies_no_vars(self, body_candidate):
        LOGGER.debug('entered generate_bodies_no_vars')
        if body_candidate.get_nb_vars() != 0:
            LOGGER.warn('body with no vars requested, but has vars: %s.', body_candidate.get_original_content())
        body = bodies.Body(body_candidate.get_combi(), body_candidate.get_original_content(),
                           positives=body_candidate.get_positive_indices(), symetric_ids=body_candidate.get_symetric_ids())
        new_bodies = self.build_negated_bodies(body)
        new_bodies.append(body)
        LOGGER.debug('left generate_bodies_no_vars')
        return new_bodies

    def _generate_bodies_with_vars(self, body_candidate):
        all_body_var_combinations, postprocessed = self._generate_body_var_combis(body_candidate)
        generated_bodies = []
        for varArrangement in all_body_var_combinations:
            reject, bodyvar_type_dict = bodies.check_body_locally(body_candidate, varArrangement)
            if reject:
                continue
            new_bodies = self._check_body_globally(body_candidate, bodyvar_type_dict, varArrangement)
            generated_bodies += new_bodies

        return generated_bodies, postprocessed

    def _generate_body_var_combis(self, body_candidate):
        combi_abstraction = body_candidate.get_abstraction()
        nb_vars = body_candidate.get_nb_vars()
        postprocessed = False
        if combi_abstraction not in self._ppCombis.keys():
            postprocessed = True
            var_rep_dict = {}
            if self._maxv is not None:
                for nb in range(2, nb_vars+1):
                    var_rep_dict[nb] = self._maxv

            if nb_vars not in self.varArrangementTrees.keys():
                self.varArrangementTrees[nb_vars] = CombiTree(nb_vars, self.var_id_generator.get_prefix(), var_rep_dict,
                                                              self._maxv)

            body_var_combis = self.varArrangementTrees[nb_vars].get_var_arrangements()[:]
            all_body_var_combinations = CombiTree.postprocess_var_arrangements(body_var_combis, body_candidate)

            self._ppCombis[combi_abstraction] = all_body_var_combinations
            # LOGGER.debug('%s var arrangements kept for %s combi.', len(all_body_var_combinations), combi_abstraction)
        else:
            all_body_var_combinations = self._ppCombis[combi_abstraction]

        return all_body_var_combinations, postprocessed

    def _get_agg_body_lengths(self, n):
        """
        :return list of tuples (len_body, len_aggregate)
        """
        agg_lengths = sorted(self._bound_dict.keys())
        body_agg_lengths = []
        for len_agg in agg_lengths:
            len_body = n - len_agg
            if len_body < 0:
                break
            elif len_body <= self._max_bodies:
                body_agg_lengths.append((len_body, len_agg))
        return body_agg_lengths

    def _get_bodies(self, n):
        bodies = self._valid_body_combis.get(n)
        if bodies is None:
            bodies = self._generate_bodies(n)
            self._valid_body_combis[n] = bodies

        return bodies

    def _init_modes_b(self, body_modes):
        self._body_modes = {}
        self._valid_body_combis = {}
        self._valid_body_combis_for_aggregate_heads = {}

        repeated_body_mode_ids = []
        max_bodies = 0
        for body in body_modes:
            modeb = ModeDeclaration(body, 'M_b')
            mode_id = modeb.get_mode_id()
            self._body_modes[mode_id] = modeb

            nb_occurrences = modeb.get_nb_occs_per_rule()
            max_bodies += nb_occurrences
            for _ in range(nb_occurrences):
                repeated_body_mode_ids.append(mode_id)

        self._body_pred_combis_per_n = bodies.build_possible_body_modeid_combis(repeated_body_mode_ids)
        self._max_bodies = max_bodies

    def _init_modes_h(self, simple_head_modes):
        self._head_single_modes = {}
        for head_declaration in simple_head_modes:
            modeh = ModeDeclaration(head_declaration, 'M_h')
            self._head_single_modes[modeh.get_mode_id()] = modeh

    def _init_modes_ha(self, agg_modes):
        self._bound_dict = heads.build_aggregate_bounds(self.max_rule_length, self._aggregate_minh, self._aggregate_maxh)
        self._head_aggregate_modes = {}
        for head_declaration in agg_modes:
            modeha = ModeDeclaration(head_declaration, 'M_ha')
            self._head_aggregate_modes[modeha.get_mode_id()] = modeha

    def _instantiate_modes(self, body_modes, h_modes, ha_modes):
        self._init_modes_b(body_modes)
        self._init_modes_h(h_modes)
        self._init_modes_ha(ha_modes)

    def _saveRule(self, n, rule):
        """adds entry to member dictionary from rule length n to the rule, or meta_rule if applicable.
        further adds entry from rule_id to normal rule string if applicable, i.e. meta_rule of the rule exists (only meta rules
        get rule ids)
        :param int n: length of rule
        :param namedtuple Rule rule: namedtuple with keywords, id, meta_str, str specifying the rule
        """
        meta_rule = rule.meta_str
        if not meta_rule:
            # some rules, like length(r1,2) are not transformed to meta rules
            self.length_to_rules_dict[n].append(rule.str)
        else:
            self.length_to_rules_dict[n].append(meta_rule)
            self.ruleID_to_rule_dict[rule.id] = rule.str

    def _setup_extra_constants(self, extra_args):
        self._aggregate_minh = extra_args.get("minh", 1)
        self._aggregate_maxh = extra_args.get("maxh", float('inf'))
        # FIXME bool(int(...)) is uhm, suboptimal
        self._multiple_head_vars_disallowed = bool(int(extra_args.get("disallow_multiple_head_vars", False)))
        self._only_constraints = bool(int(extra_args.get("only_constraints", False)))
        self._maxv = extra_args.get("maxv")
        self.max_rule_length = extra_args['max_rule_length']
