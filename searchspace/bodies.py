from collections import defaultdict
import copy
import itertools

import tricks
import utils
from utils.logger import IlaspLogger
from utils.rules_utils import PredicateAccessor

LOGGER = IlaspLogger.getLogger(__name__)


def build_negated_pred_combis(body):
    """generates for given body all possible derivable negated bodies.
    :param list of str list_body_str
    :param list of str list_body_vars: order of variables corresponding to list_body_str
    :param list of int non_negatable_preds: indices of positive predicates wrt same order as list_body_str and list_body_vars
    """
    non_negatable_preds = body.get_positive_indices()
    list_body_str = body.get_replaced_preds()
    list_body_vars = body.get_vars_str()
    nb_preds = len(list_body_str)
    all_negations = []
    var_to_pred_dict = defaultdict(list)
    #  map variables to list of predicates in which they occur
    for idx_pred, pred_vars in enumerate(list_body_vars):
        for var in set(pred_vars):
            var_to_pred_dict[var].append(idx_pred)

    forbidden_negations = var_to_pred_dict.values()
    for nb_negated in range(1, len(list_body_str) + 1):
        possible_negation_combis = list(itertools.combinations(range(nb_preds), nb_negated))
        for negation in possible_negation_combis:
            if any([set(forbidden_combi).issubset(negation) for forbidden_combi in forbidden_negations]):
                continue
            elif any(elem in non_negatable_preds for elem in negation):
                continue
            else:
                all_negations.append(negation)

    return all_negations


def build_possible_body_modeid_combis(repeated_modes):
    """
    :param list repeated_modes: ids of all possible body modes, each appears as often as the maximal number of times it can
    occur inside one body
    :return list of tuples of strings: each tuple contains a combination of mode ids
    """
    body_combis = defaultdict(list)
    for nb_body_preds in range(1, len(repeated_modes) + 1):
        body_pred_combis = set(itertools.combinations(repeated_modes, nb_body_preds))
        body_combis[nb_body_preds] = list(body_pred_combis)

    return body_combis


def check_body_locally(body_candidate, varArrangement):
    bodyvar_type_dict = {}
    reject = tricks.check_symmetry(body_candidate, varArrangement)
    if not reject:
        reject = tricks.check_anti_reflexive(body_candidate, varArrangement)
        if not reject:
            reject, bodyvar_type_dict = tricks.check_types(varArrangement, body_candidate.get_vtypes())

    return reject, bodyvar_type_dict


def convert_body_to_meta(rule_id, body, extra_bodies=None):
    if body is None:
        return 'active(' + str(rule_id) + '), ex(X)'

    meta_body_literals = []
    body_elems = body.get_replaced_preds()
    # LOGGER.debug('pos body preds: %s', body_elems)
    neg_indices = body.get_indices_negated_preds()
    for idx_pred, body_literal in enumerate(body_elems):
        meta_body_str = 'e(' + body_literal.strip() + ', X)'
        if neg_indices is not None and idx_pred in neg_indices:
            meta_body_str = 'not ' + meta_body_str

        meta_body_literals.append(meta_body_str)

    if extra_bodies is not None:
        meta_body_literals += extra_bodies

    # FIXME potential bug? check this we always need active(ruleid), ext(X) if we have at last one rewritten atom
    if not meta_body_literals:
        return ''
    else:
        # always need active(ruleid), ex(X) if we have at last a single rewritten atom
        meta_body_literals.append('active(' + str(rule_id) + '), ex(X)')
        return ','.join(meta_body_literals)


class Body(object):

    def __init__(self, combi, replaced_preds, positives=None, symetric_ids=[], vtype_dict={}, indices_neg_preds=[]):
        self._content = replaced_preds
        # LOGGER.debug('INIT BODY; replaced_preds: %s', replaced_preds)
        self._set_body_vars()
        self._positives = positives
        self._symetric_ids = symetric_ids
        self._negative_indices = indices_neg_preds
        self._vtype_dict = vtype_dict
        self._combi = combi
        self._indices_redundant_dups = []

    def get_combi(self):
        return self._combi

    def get_indices_negated_preds(self):
        return list(self._negative_indices)

    def get_positive_indices(self):
        return list(self._positives)

    def get_redundant_dups(self):
        return list(self._indices_redundant_dups)

    def get_replaced_preds(self):
        """never contains negated predicates, negation can be done on the fly with negated indices
        """
        return list(self._content)

    def get_symetric_ids(self):
        return list(self._symetric_ids)

    def get_symetric_var_mapping(self):
        all_vars = self.get_vars_str()
        mapping = {}
        for mid in self._symetric_ids:
            sym_indices = utils.get_all_indices_by_value(self._combi, [mid])
            for idx in sym_indices.values()[0]:
                # TODO: One direction should be enough?!
                v1 = all_vars[idx][0]
                v2 = all_vars[idx][1]
                mapping[v1] = v2
                mapping[v2] = v1
        return mapping

    def get_vars_str(self):
        """
        :return list of lists containing variables of respective body predicates
        """
        return copy.deepcopy(self._list_str_vars)

    def get_vtype_dict(self):
        # TODO: deep copY!
        return self._vtype_dict

    def set_redundant_dups(self, indices):
        """
        :param list of tuples indices
        """
        self._indices_redundant_dups = indices

    def _set_body_vars(self):
        predAcc = PredicateAccessor(self.get_replaced_preds())
        self._list_str_vars = predAcc.get_vars_str()
