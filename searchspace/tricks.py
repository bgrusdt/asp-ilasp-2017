from collections import defaultdict
import itertools
import numpy as np

from utils.logger import IlaspLogger
from asp_representation.modes import ModeDeclaration
import utils
import utils.ilasp_utils
import varstructures

logger = IlaspLogger.getLogger(__name__)


def check_anti_reflexive(body_candidate, varArrangement):
    """if a 2-ary predicate is anti-reflexive, it means that the two variables must not be equal, e.g. a(X,X) is not allowed.
    :param body_candidate
    :param varArrangement: list of str containing variables of entire body, e.g. [V0,V0,V1,V0,V2]
    In examples given above, if V0,V0 are variables of an anti-reflexive predicate (anti-reflexives: [[0,1],[2,3]]) and therefore
    the arrangement will be rejected.
    """
    anti_ref_preds = body_candidate.get_anti_ref_indices()
    anti_reflexive_vars = body_candidate.get_var_indices(anti_ref_preds)
    reject = False
    # 1. anti-reflexives
    for anti_pair in anti_reflexive_vars:
        # anti_pair always refers to two variables only, i.e. e.g. (1,2)
        if len(anti_pair) != 2:
            logger.warn('anti-reflexive predicates must be 2-ary.')
        elif(varArrangement[anti_pair[0]] == varArrangement[anti_pair[1]]):
            reject = True
            break

    return reject


def check_body_duplicates(body):
    """ makes checks on predicates that appear more than once in this body:
    - They must not be equal wrt all their variables, e.g. a(V0,V1), a(V0,V1).
    - They must not have completely different variables that do not appear elsewhere in the body, e.g. a(V0,V1), a(V2,V3), c(V4).
    - checks for all variables of all duplicate predicates if they appear always at the same slot and only in the duplicate
    predicates and nowhere else, e.g. a(V0,V1), a(V0,V2) is discarded, but a(V0,V1), a(V2,V0) is kept.
    - further checks on duplicate predicates where one predicate has only one unique variable at least twice,
    e.g. a(V0,V0,V0), a(V0,V1,V2), c(V3). Checks if none of the variables of the duplicate predicates (except that unique
    variable), appears anywhere else in the body. If so, the rule is discarded. Since if a(V0,V1) is True, a(V0,V0) will always
    be True as well and to keep the exact same meaning, V1 must not appear anywhere else in the body.
    :param Body body
    :return special aggregate bodies or None
            None if the rule is not valid
            [] if the rule is valid for aggregate rules, single head rules and constraints and shall be kept
            list of tuple [(2,(..))] if the rule is valid for aggregate rules but not for all other rules, e.g. q(V0), q(V1).
    :rtype list
    """
    combi = body.get_combi()
    duplicate_indices = (utils.get_duplicates(combi)).values()
    if(not duplicate_indices):
        return []

    body_parts = body.get_replaced_preds()
    body_vars = body.get_vars_str()

    # 1. equal wrt all variables e.g. a(V0,V0), a(V0,V0) - discard completely
    identical_preds = utils.get_duplicates(body_parts)
    if(identical_preds):
        return None

    all_different = False
    # 2. Check if all other variables of the body appear anywhere else other than in the same duplicate predicates.
    # keep the rule if that's the case; otherwise discard it
    results = []
    for dup_indices in duplicate_indices:
        nb_vars_dup_pred = len(body_vars[dup_indices[0]])
        dup_var_set = set()
        all_duplicate_vars = []
        nb_dup_vars = 0

        one_var_occ_preds = []
        for i, idx_pred in enumerate(dup_indices):
            current_vars = body_vars[idx_pred]
            nb_dup_vars += len(current_vars)
            if(nb_vars_dup_pred > 1 and len(set(current_vars)) == 1):
                one_var_occ_preds.append((i, current_vars[0]))

            dup_var_set = dup_var_set.union(current_vars)
            all_duplicate_vars.append(current_vars)

        if(nb_dup_vars == len(dup_var_set)):
            all_different = True

        all_other_vars = np.delete(body_vars[:], dup_indices, 0)
        all_other_vars = list(utils.flattened_iter(all_other_vars))
        # Check if there is a duplicate predicate that contains one variable only several times, e.g in body a(V0,V0), a(V0,V1)
        if one_var_occ_preds:
            # does any of the duplicates' vars that is not equal to the single var (here V0) occur elsewhere in body?
            # if there are no other variables in the body, we keep the rule only for rules with aggregate heads
            if not all_other_vars:
                return [dup_indices]

            nb_var_elsewhere = 0
            for idx_one_var_pred, single_var in one_var_occ_preds:
                dup_vars = all_duplicate_vars[:]
                del dup_vars[idx_one_var_pred]
                flattened_dup_vars = [elem for sublist in dup_vars for elem in sublist if elem != single_var]

                for var in flattened_dup_vars:
                    if var in all_other_vars:
                        nb_var_elsewhere += 1

            if nb_var_elsewhere == len(one_var_occ_preds):
                results.append((1, ()))
            else:
                results.append((2, dup_indices))

        elif all_different:
            var_elsewhere = False
            for var in dup_var_set:
                if var in all_other_vars:
                    var_elsewhere = True
                    results.append((1, ()))
                    break
            if not var_elsewhere:
                results.append((2, dup_indices))
        else:
            # e.g. a(V0,V1), a(V0,V2) or a(V0,V1), a(V1,V0)
            # sublists need to have same length for numpy
            max_vars = max(map(len, body_vars))
            for sublist in body_vars:
                for _ in range(len(sublist), max_vars):
                    sublist.append('')
            bodyvars_array = np.array(body_vars)

            nvars_ok = 0
            for var in dup_var_set:
                preds, args = np.where(bodyvars_array == var)
                if(len(set(args)) == 1):
                    # the variable occurs always at the same slot
                    if(set(preds.tolist()).issubset(set(dup_indices))):
                        # and only in duplicate predicates (any)
                        nvars_ok += 1

            if nvars_ok != len(dup_var_set):
                results.append((1,))
            else:
                results.append((2, dup_indices))

    special_bodies = [ttuple[1] for ttuple in results if ttuple[0] == 2]

    logger.debug('special bodies: %s', special_bodies)
    return special_bodies


def check_global_symmetry(body, body_candidate):
    """creates a set of forbidden rules which do not include the original rule, but only different other rules that are
    semantically identical to the original and should therefore not be added to the searchspace. It is checked for
    symmetric, as defined in ILASP paper.
    """
    symetric_ids = body_candidate.get_symetric_ids()
    combi = body_candidate.get_combi()
    original_content = body_candidate.get_original_content()
    forbidden_rules = set()
    body_vars = body.get_vars_str()
    body_vars_flattened = [elem for pred in body_vars for elem in pred]
    combinations = []

    for idx, mid in enumerate(combi):
        current_pred_vars = [body_vars[idx]]
        if mid in symetric_ids:
            current_pred_vars.append([body_vars[idx][1], body_vars[idx][0]])

        combinations.append(current_pred_vars)

    identical_var_combis = list(itertools.product(*combinations))
    if(len(identical_var_combis) > 1):
        for var_combi in identical_var_combis:
            flattened_vars = [elem for pred in var_combi for elem in pred]
            # abstract only if not all variables are different, then abstraction is always the same
            if(len(set(flattened_vars)) != len(flattened_vars)):
                abstracted_vars = varstructures.get_abstracted_items(flattened_vars)
                # only add this combination to forbidden rules if its not equal to original combination
                if(body_vars_flattened != abstracted_vars):
                    replaced = (utils.ilasp_utils).replace_pattern_in_predicates(original_content, abstracted_vars,
                                                                                 ModeDeclaration.var_pattern)
                    # if(replaced):
                    forbidden_rules.add(",".join(replaced))

    return forbidden_rules


def check_symmetry(body_candidate, var_arrangement):
    """checks if var arrangement contains at symmetric duplicate predicates symmetric variables, e.g. a(V0,V1) and a(V1,V0).
    :param list of str var_arrangement
    :param dict sym_dup_indices: maps from mode_id to a list of 2-ary tuples containing the variable indices
    of the respective occurrences of that symmetric predicate in the body
    """
    dup_dict = body_candidate.get_duplicates_dict()
    symetric_ids = body_candidate.get_symetric_ids()
    sym_dup_indices = {}
    for mid, pred_indices in dup_dict.iteritems():
        if mid in symetric_ids:
            var_indices = body_candidate.get_var_indices(pred_indices)
            sym_dup_indices[mid] = var_indices

    reject = False
    forbidden_var_combis = defaultdict(list)
    actual_combis = defaultdict(list)
    for mid, indices in sym_dup_indices.iteritems():
        if any(elem != 2 for elem in map(len, indices)):
            logger.error('Symmetry checked for a predicate that is not 2-ary; only 2-ary predicates can be symmetric.')
            raise ValueError('only 2-ary predicates can be checked for symmetry')
        for idx_fst, idx_snd in indices:
            fst_var = var_arrangement[idx_fst]
            snd_var = var_arrangement[idx_snd]
            forbidden_var_combis[mid].append((snd_var, fst_var))
            actual_combis[mid].append((fst_var, snd_var))

        for mid, combis in actual_combis.iteritems():
            for combi in combis:
                if combi in forbidden_var_combis[mid]:
                    return True

    return reject


def check_types(var_arrangement, vartypes):
    """
    :param list of str var_arrangement
    :param list of str vartypes
    """
    required_vartypes_dict = defaultdict(str)
    var_type_dict = defaultdict(str)
    reject = False
    for idx, var in enumerate(var_arrangement):
        real_type = vartypes[idx]

        if(var not in required_vartypes_dict.keys()):
            required_vartypes_dict[var] = vartypes[idx]

        required_type = required_vartypes_dict[var]
        if(required_type != real_type):
            reject = True
            var_type_dict = defaultdict(str)
            break
        else:
            var_type_dict[var] = required_type

    return reject, var_type_dict
