from collections import defaultdict


ASP_NAME_OF_EX_ATOM = "ex"
ASP_NAME_OF_EX_VAR = "__EX__"
ASP_NAME_OF_E = "e"
ASP_NAME_OF_ACTIVE = "active"

SUBPRG_META = "meta"
SUBPRG_BACKGROUND_KNOWLEDGE = "background"
SUBPRG_POSITIVE_EXAMPLES = "posExamples"
SUBPRG_NEGATIVE_EXAMPLES = "negExamples"
SUBPRG_CHOOSE_ACTIVES = "choose"
SUBPRG_STEP = "step"
SUBPRG_EXNEG = "ilaspExNeg"
SUBPRG_SEARCHSPACE = "sm"

DEFAULT_MAX_HYPOTHESIS_COST = 15

# how to name generated rules and variables
RULE_PREFIX = 'r'
VAR_PREFIX = 'V'

SOLUTION_LOGGER_NAME = 'solutionLogger'
VIOLATING_LOGGER_NAME = 'violatingLogger'
