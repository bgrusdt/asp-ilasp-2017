from utils import *
from utils_for_tests import expect_failure


def test_do_if():
    list = []
    func = do_if(lambda x: x > 10, list.append)
    assert func(20)
    assert not func(10)
    assert func(50)
    assert [20,50] == list


def test_replace():
    assert "aXbYZc" == replace("a$V$b$V$$V$c", "$V$", ["X","Y","Z"]);
    assert "abcdef" == replace("xxxxxx", "x", "abcdef")
    assert "ab--ee" == replace("abRR-RRe", "RR", ["-","e"])


def test_replace_mismatch_pattern_count():
    expect_failure(lambda: replace("V","V",["A","B"]), IndexError)
    expect_failure(lambda: replace("VV", "V", ["A"]), IndexError)


def test_index_multiple():
    assert ["a","e"] == index_multiple("abcdefghij", (0, 4))


def test_get_indices_for_duplicates():
    values = [1,1,2,3,4,5,6,3,4,8]
    assert [0, 1, 3, 4, 7, 8] == get_indices_for_duplicates(values)