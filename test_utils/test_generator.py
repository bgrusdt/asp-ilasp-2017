from utils.generators import IDGenerator

PREFIX="prefixo"

def test_generator():
    gen = IDGenerator(prefix=PREFIX)
    assert PREFIX == gen.get_prefix()
    cid = gen.nextId()
    assert cid == PREFIX+"1"
    assert cid == gen.get_currentId()
    assert 1 == gen.get_current_id_number()

    cid = gen.nextId()
    assert cid == PREFIX + "2"
    assert cid == gen.get_currentId()
    assert 2 == gen.get_current_id_number()

    gen.reset_ID()
    cid = gen.get_currentId()
    assert cid == PREFIX + "1"
    assert cid == gen.get_currentId()
    assert 1 == gen.get_current_id_number()

