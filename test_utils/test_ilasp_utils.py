import clingo
from utils.ilasp_utils import *


def test_extract_actives():
    ctl = clingo.Control(['1'])
    ctl.add("base",[], "active(1..3).")
    ctl.ground([("base",[])])

    res = set()
    with ctl.solve_iter() as solutions:
        for model in solutions:
            for active in extract_actives(model):
                res.add(str(active))

    assert { "active(1)", "active(2)", "active(3)" } == res


def test_extract_bad_actives_too():
    ctl = clingo.Control(['1'])
    ctl.add("base", [], "active(that, is, bad).")
    ctl.ground([("base", [])])

    res = set()
    with ctl.solve_iter() as solutions:
        for model in solutions:
            for active in extract_actives(model):
                res.add(str(active))

    assert {"active(that,is,bad)", } == res
