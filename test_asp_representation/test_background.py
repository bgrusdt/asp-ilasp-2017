from asp_representation.background import Background
from utils_for_tests import as_str_set
from constants import SUBPRG_BACKGROUND_KNOWLEDGE


def bg_test(inp, outs):
    res = [ rule.prg_content_string for rule in Background.from_str(inp).as_rules() ]
    assert as_str_set(outs) == as_str_set(res)


def test_uses_right_background_name():
    res = Background.from_str("bla.")
    assert {
        "#program {}.".format(SUBPRG_BACKGROUND_KNOWLEDGE),
        "e(bla,__EX__) :- ex(__EX__)."
    } == as_str_set(res._program)


def test_bg_simple():
    bg_test("something.",["e(something,__EX__) :- ex(__EX__)."])


def test_bg_complex():
    bg_test(
        """
        :- a(X), X > 10.
        { a; b } :- e(X), X < 20; X >= 5.
        a(X) :- e(X) : c(X,R),d(X); e(R).
        :- 3 < #count { a(R) : b(R) }.
        """,
        [
            "#false :- e(a(X),__EX__); X>10; ex(__EX__).",
            "{ e(a,__EX__) : ; e(b,__EX__) :  } :- e(e(X),__EX__); X<20; X>=5; ex(__EX__).",
            "e(a(X),__EX__) :- e(e(X),__EX__) : e(c(X,R),__EX__), e(d(X),__EX__); e(e(R),__EX__); ex(__EX__).",
            "#false :- 3 < #count { e(a(R),__EX__) : e(b(R),__EX__) }; ex(__EX__)."
        ]
    )


def test_bg_negation():
    bg_test(
        """
        :- not a(X); b(X).
        not e(X) :- a(X).
        """,
        [
            "#false :- not e(a(X),__EX__); e(b(X),__EX__); ex(__EX__).",
            "not e(e(X),__EX__) :- e(a(X),__EX__); ex(__EX__)."
        ]
    )
