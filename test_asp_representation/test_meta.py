import clingo

from utils.generators import IDGenerator
from utils.ilasp_utils import TypeManager
from asp_representation.background import Background
from data import Examples
from asp_representation.meta import *
from data import ProblemInstance

from utils_for_tests import rules, rules_with_param


def create_test_type_manager():
    return TypeManager({"number": [1,2,3,4]})


def test_prog_parts_iter():
    mrules = MetaRules([
        [StrRule("test",[],"a(1).")],
        [StrRule("test2", [], ":- a(1).")]
    ])
    parts = list(mrules.prog_parts_iter())
    assert parts, [("test",[],"a(1)."), (":- a(1).")]


def test_construct_meta_background():
    background = Background.from_str("a(X) :- b(X).")
    type_manager = create_test_type_manager()
    result = constructMetaBackground( background, type_manager )
    #FIXME 1. make all occurences of prg_vars a tuple, 2. use set here
    #program background. e(a(X),__EX__) :- e(b(X),__EX__); ex(__EX__).')
    expected = rules( "background", [
        'e(a(X),__EX__) :- e(b(X),__EX__); ex(__EX__).',
        #TODO reconsider/check if this is what we expect here
        'number(1).',
        'number(2).',
        'number(3).',
        'number(4).'
    ])
    assert expected == result


def test_constructMetaExamples_empty():
    result = constructMetaExamples(Examples(pos=[], neg=[]), IDGenerator('ex'))
    assert [] == result


def test_constructMetaExamples_pos():
    examples = Examples( pos=[
        {
            "inclusions" : [ "a(1,2)", "b(1,2)" ],
            "exclusions" : ["c(1,2)"]
        },
        {
            "inclusions": ["a(1,2)"],
            "exclusions": ["c(1,2)", "b(1,2)"]
        }
    ], neg=[])

    result = constructMetaExamples(examples, IDGenerator('ex'))
    expected = rules( "posExamples", [
        'ex(ex1).',
        ':- not covered(ex1).',
        'covered(ex1) :-  e(a(1,2),ex1), e(b(1,2),ex1), not e(c(1,2),ex1).',
        "ex(ex2).",
        ":- not covered(ex2).",
        'covered(ex2) :-  e(a(1,2),ex2), not e(c(1,2),ex2), not e(b(1,2),ex2).'
    ])
    assert expected == result


def test_constructMetaExamples_neg():
    examples = Examples(pos=[], neg=[
        {
            "inclusions" : [ "a(1,2)", "b(1,2)" ],
            "exclusions" : ["c(1,2)"]
        },
        {
            "inclusions": ["a(1,2)"],
            "exclusions": ["b(1,2)", "c(1,2)"]
        }
    ])
    result = constructMetaExamples(examples, IDGenerator('ex'))
    expected = rules( "negExamples", [
        'violating :-  e(a(1,2),neg), e(b(1,2),neg), not e(c(1,2),neg).',
        'violating :-  e(a(1,2),neg), not e(b(1,2),neg), not e(c(1,2),neg).'
    ])
    assert expected == result


def test_constructMetaExamples_pos_neg():
    example = Examples(
        pos = [{
            "inclusions": ["a(1,2)"],
            "exclusions": ["b(1,2)", "c(1,2)"]
        }],
        neg= [{
            "inclusions" : [ "a(1,2)", "b(1,2)" ],
            "exclusions" : ["c(1,2)"]
        }]
    )
    result = constructMetaExamples(example, IDGenerator('ex'))
    expected = rules( "posExamples", [
        'ex(ex1).',
        ':- not covered(ex1).',
        'covered(ex1) :-  e(a(1,2),ex1), not e(b(1,2),ex1), not e(c(1,2),ex1).'
    ]) + rules( "negExamples", [
        'violating :-  e(a(1,2),neg), e(b(1,2),neg), not e(c(1,2),neg).'
    ])
    assert expected == result


def test_construct_meta_aux_choiceRule():
    result = construct_meta_aux_choiceRule()
    expected = [
        StrRule("choose", ["n"], "{active(X)} :- X=@getRuleIds()."),
        StrRule("choose", ["n"], ":- not #sum {X,R : active(R), length(R,X)} == n , step(n)."),
        StrRule("choose", ["n"], ":-  ex(neg), not violating.")
    ]
    assert expected == result


def test_constructStep():
    result = constructStep()
    expected = [
        StrRule(prg_name="step", prg_vars=["t"], prg_content_string="step(t).")
    ]
    assert expected == result


def test_constructIlaspExNeg():
    #FIXME as we don't have incremental solving any more we don't need externals anymore
    result = constructIlaspExNeg()
    expected = [
        StrRule(prg_name="ilaspExNeg", prg_vars=["t"], prg_content_string="#external ex(t).")
    ]
    assert expected == result


def test_constructMetaRules():
    problem = ProblemInstance.from_paths(
        example_path="./test_resources/simple_examples.json",
        background_path="./test_resources/simple_background.lp",
        mode_declaration_path="./not/used/in/this/test",
        type_manager=create_test_type_manager()
    )

    result = constructMetaRules(problem, IDGenerator('ex'))
    #FIMXE we dont really need a array of array
    expected = MetaRules([
        rules( "background", [
            'e(a(X),__EX__) :- e(b(X),__EX__); ex(__EX__).',

            'number(1).',
            'number(2).',
            'number(3).',
            'number(4).'
        ]),
        rules( "posExamples", [
            'ex(ex1).',
            ':- not covered(ex1).',
            #FIXME BUG for some reason this was unicode (u'...')
            'covered(ex1) :-  e(a(1,2),ex1), not e(b(1,2),ex1), not e(c(1,2),ex1).'
        ]) + rules( "negExamples", [
            'violating :-  e(a(1,2),neg), e(b(1,2),neg).',
            'violating :-  e(a(1,2),neg), e(c(1,2),neg).'
        ]),
        rules_with_param('choose', ['n'], [
            '{active(X)} :- X=@getRuleIds().',
            ':- not #sum {X,R : active(R), length(R,X)} == n , step(n).',
            ':-  ex(neg), not violating.'
        ]),
        #FIXME t? not n?
        rules_with_param( "step", ['t'], [
            'step(t).'
        ]),
        # FIXME t? not n?
        rules_with_param( 'ilaspExNeg', ['t'], [
            '#external ex(t).'
        ])
    ])
    assert expected.data == result.data

