from six.moves import range
import time
import clingo

from utils.generators import IDGenerator
from utils.logger import IlaspLogger
from asp_representation.meta import constructMetaRules


from constants import (
    RULE_PREFIX,
    SOLUTION_LOGGER_NAME,
    VIOLATING_LOGGER_NAME,
)

LOGGER = IlaspLogger.getLogger('ilasp')


class Context(object):
    def __init__(self, rule_ids):
        self._rule_ids = rule_ids

    def getRuleIds(self):
        return [clingo.Function(rule_id) for rule_id in self._rule_ids]


class ILASP(object):

    def __init__(self, problem, searchspace):
        """
        """
        # build the meta program from the given resources
        self._problem = problem
        self._search_space = searchspace
        self.meta_builder = constructMetaRules(problem, IDGenerator('ex'))

        self.violating_logger = IlaspLogger.getLogger(VIOLATING_LOGGER_NAME)  # RM
        self.solution_logger = IlaspLogger.getLogger(SOLUTION_LOGGER_NAME)

    def lookup_rule(self, rule_id):
        return self._search_space.get_rule_by_id(rule_id)

    def get_old_rules(self, upto_length):
        """ fetches already generated rules up to given length
        :param int upto_length: maximal length of rules to be retrieved
        :returns each sublist contains rules of given length
        :rtype list of list of str rules
        """
        rules = []
        if upto_length > 0:
            LOGGER.info('fetch ILASP rules of lengths up to %s from previous iterations.', upto_length)
            # Note: it's six range, not py2's range (i.e. it's xrange on py2, range on py3)
            for rule_length in range(1, upto_length + 1):
                rules.append(self._search_space.get_rules_by_length(rule_length))

        LOGGER.info('previous rules fetched.')
        return rules

    def only_build_search_space(self, up_to_length=None):
        """

        :param up_to_length:
        :return: a iterator over pairs of rule length and a iterator over rules of this length
        """
        LOGGER.info('ONLY GENERATING SEARCHSPACE')
        if up_to_length is None:
            up_to_length = self._search_space.max_rule_length

        # we want to non, wrapped rules without any meta rules like length
        # because of how SearchSpace is curently implemented getting this
        # is a bit round about
        grouped_rids = []
        for rule_length in range(up_to_length + 1):
            rids, _all_rules = self._search_space.build_search_space(rule_length)
            grouped_rids.append((rule_length, rids))

        return (
            (length, (self._search_space.get_rule_by_id(rid) for rid in rids))
            for (length, rids) in grouped_rids
        )

    def run_ilasp(self, max_cost=15):
        raise NotImplementedError("expected to be overriden with concrete implementation")
