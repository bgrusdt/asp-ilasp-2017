import time
import clingo

import constants
from utils import flattened_iter
from utils.profile import profile, profile_section, get_active_profiling_section
from ilasp_control import Control
from algorithm.skeleton import ILASP as _ILASP
from algorithm.skeleton import Context, LOGGER


from constants import (
    SUBPRG_SEARCHSPACE,
    SUBPRG_STEP,
    SUBPRG_EXNEG,
    SUBPRG_BACKGROUND_KNOWLEDGE,
    SUBPRG_NEGATIVE_EXAMPLES,
    SUBPRG_POSITIVE_EXAMPLES,
    SUBPRG_CHOOSE_ACTIVES,
    SUBPRG_META
)


def validator(_solution):
    # the section which found the solution (clingo solve next) is already concluded
    # so when this is called we need to add the metadatum to the last subsection,
    # which we know has to exist, as there was a solution
    get_active_profiling_section().add_metadatum("is_solution")
    return True


class ILASP(_ILASP):


    def iteration_step(self, cost, rule_ids):
        start_time = time.time()
        LOGGER.debug('entering ILASP loop for rules of length %s...', cost)

        prog_instance = Control(["0", "--project"])

        for (name, variables, content) in self.meta_builder.prog_parts_iter():
            prog_instance.add(name, variables, content)

        if cost > 1:
            old_rules = self.get_old_rules(cost-1)
            prog_instance.add_many(SUBPRG_SEARCHSPACE, [], flattened_iter(old_rules))

        # FIXME rules in two different formats...
        # build searchspace and get_old_rules returns different types,
        # get old rules returns List[List[str]], while build_search_space
        # returns List[List[Rule]]
        # for now this is fixed in add_many add hoock, but bad style as add_many
        # shouldn't know about the inernals of searchspace.Rule
        new_rule_ids, new_rules = self._search_space.build_search_space(cost)
        prog_instance.add_many(SUBPRG_SEARCHSPACE, [], new_rules)
        rule_ids += new_rule_ids
        context = Context(rule_ids)
        with profile_section('ILASP.ground'):
            prog_instance.ground([("base", []),
                                  (SUBPRG_META, []),
                                  (SUBPRG_STEP, [cost]),
                                  (SUBPRG_EXNEG, [clingo.Function("neg")]),
                                  (SUBPRG_BACKGROUND_KNOWLEDGE, []),
                                  (SUBPRG_NEGATIVE_EXAMPLES, []),
                                  (SUBPRG_POSITIVE_EXAMPLES, []),
                                  (SUBPRG_SEARCHSPACE, []),
                                  (SUBPRG_CHOOSE_ACTIVES, [cost])], context)
        prog_instance.run_negative_phase(context, cost)
        solution_sets = prog_instance.run_positive_phase(cost, validator)

        duration = time.time() - start_time
        LOGGER.info('duration for length %s was %s.', cost, duration)
        LOGGER.debug('leaving loop for length %s.', cost)
        # cost += 1

        return solution_sets, rule_ids

    @profile("ILASP.run_ilasp", metadata=["iter_neg_pos"])
    def run_ilasp(self, max_hypothesis_cost=15):
        """ computes solution for given problem instance
        :param int max_hypothesis_cost: the maximal cost of a hypothesis
        :param bool reuse_consts: if True, in each iteration, the constraints learned in previous iteration will be
        taken into account for current iteration
        :return list of list of str solutionRules: for each solution one sublist, which contains the rules forming that solution
        and list of str ps: entire model strings of the solutions
        """
        LOGGER.info('subprogram names:')
        LOGGER.info(SUBPRG_BACKGROUND_KNOWLEDGE)
        LOGGER.info(SUBPRG_CHOOSE_ACTIVES)
        LOGGER.info(SUBPRG_EXNEG)
        LOGGER.info(SUBPRG_NEGATIVE_EXAMPLES)
        LOGGER.info(SUBPRG_POSITIVE_EXAMPLES)
        LOGGER.info(SUBPRG_SEARCHSPACE)
        LOGGER.info(SUBPRG_STEP)
        LOGGER.info(SUBPRG_META)

        rule_ids = []

        cost = 1
        solution_sets = set()
        while not solution_sets and cost <= max_hypothesis_cost:
            with profile_section("ILASP.iteration_step[{}]".format(cost-1)):
                solution_sets, rule_ids = self.iteration_step(cost, rule_ids)
            cost += 1

        self.solution_logger.info("found %s solutions", len(solution_sets))
        for solution in solution_sets:
            self.solution_logger.info("solution: %s", ", ".join(sorted(solution)))

        return solution_sets
