import time
import clingo
from itertools import chain
import constants
from utils import flattened_iter
from utils.profile import profile_section, profile, get_active_profiling_section
from ilasp_control import Control, rule_as_str
from algorithm.skeleton import ILASP as _ILASP
from algorithm.skeleton import Context, LOGGER


from constants import (
    SUBPRG_SEARCHSPACE,
    SUBPRG_STEP,
    SUBPRG_EXNEG,
    SUBPRG_BACKGROUND_KNOWLEDGE,
    SUBPRG_NEGATIVE_EXAMPLES,
    SUBPRG_POSITIVE_EXAMPLES,
    SUBPRG_CHOOSE_ACTIVES,
    SUBPRG_META
)

class ILASP(_ILASP):

    def iteration_step(self, cost, rule_ids):
        start_time = time.time()
        LOGGER.debug('entering ILASP loop for rules of length %s...', cost)

        prog_instance = Control(["0", "--project"])

        for (name, variables, content) in self.meta_builder.prog_parts_iter():
            prog_instance.add(name, variables, content)

        if cost > 1:
            old_rules = self.get_old_rules(cost-1)
            prog_instance.add_many(SUBPRG_SEARCHSPACE, [], flattened_iter(old_rules))

        # FIXME rules in two different formats...
        # build searchspace and get_old_rules returns different types,
        # get old rules returns List[List[str]], while build_search_space
        # returns List[List[Rule]]
        # for now this is fixed in add_many add hoock, but bad style as add_many
        # shouldn't know about the inernals of searchspace.Rule
        new_rule_ids, new_rules = self._search_space.build_search_space(cost)
        prog_instance.add_many(SUBPRG_SEARCHSPACE, [], new_rules)
        rule_ids += new_rule_ids
        context = Context(rule_ids)
        ground_start_time = time.time()
        with profile_section('ILASP.ground'):
            prog_instance.ground([("base", []),
                                  (SUBPRG_META, []),
                                  (SUBPRG_STEP, [cost]),
                                  (SUBPRG_EXNEG, [clingo.Function("neg")]),
                                  (SUBPRG_BACKGROUND_KNOWLEDGE, []),
                                  (SUBPRG_NEGATIVE_EXAMPLES, []),
                                  (SUBPRG_POSITIVE_EXAMPLES, []),
                                  (SUBPRG_SEARCHSPACE, []),
                                  (SUBPRG_CHOOSE_ACTIVES, [cost])], context)
        ground_time = time.time() - ground_start_time
        valid_solution_sets = prog_instance.run_positive_phase(cost, self.validate_solution)

        duration = time.time() - start_time
        LOGGER.info('duration for length %s was %s.', cost, duration)
        LOGGER.debug('leaving loop for length %s.', cost)
        return valid_solution_sets, rule_ids

    @profile("ILASP.validate_solution")
    def validate_solution(self, solution):
        validator = Control(["1"])
        # 1. add background knowledge
        validator.add("base", [], self._problem.background._source)

        # 2. add negative example
        for nexample in self._problem.examples.neg:
            rule = "bad :- {}.".format(", ".join(chain(
                nexample['inclusions'],
                ("not " + v for v in nexample["exclusions"])
            )))
            validator.add("base", [], rule)
        # try to find bad thinks so make sure bad is included.
        validator.add("base", [], ":- not bad.")

        # 3. add found rules
        for rule_id in solution:
            rule = self.lookup_rule(rule_id)
            validator.add("base", [], rule_as_str(rule))
        with profile_section("ILASP.validate_solution.ground"):
            validator.ground([("base", [])])

        # Yes, unsatisfiable returns True, i.e. it's a valid solution
        valid = bool(validator.solve().unsatisfiable)
        if valid:
            # we already left the clingo solver next section so to attach a metadatum
            # to it we need to get it, it's the last child of the current active
            # section
            get_active_profiling_section().add_metadatum("is_solution")
        return valid

    @profile("ILASP.run_ilasp", metadata=["iter_pos_validate"])
    def run_ilasp(self, max_hypothesis_cost=15):
        """ computes solution for given problem instance
        :param int max_hypothesis_cost: maximal cost of a hypothesis
        :return list of list of str solutionRules: for each solution one sublist, which contains the rules forming that solution
        and list of str ps: entire model strings of the solutions
        """
        LOGGER.info('subprogram names:')
        LOGGER.info(SUBPRG_BACKGROUND_KNOWLEDGE)
        LOGGER.info(SUBPRG_CHOOSE_ACTIVES)
        LOGGER.info(SUBPRG_EXNEG)
        LOGGER.info(SUBPRG_NEGATIVE_EXAMPLES)
        LOGGER.info(SUBPRG_POSITIVE_EXAMPLES)
        LOGGER.info(SUBPRG_SEARCHSPACE)
        LOGGER.info(SUBPRG_STEP)
        LOGGER.info(SUBPRG_META)

        rule_ids = []

        cost = 1
        solution_sets = set()
        while not solution_sets and cost <= max_hypothesis_cost:
            with profile_section("ILASP.iteration_step[{}]".format(cost - 1)):
                solution_sets, rule_ids = self.iteration_step(cost, rule_ids)
            cost += 1

        self.solution_logger.info("found %s solutions", len(solution_sets))
        for solution in solution_sets:
            self.solution_logger.info("solution: %s", ", ".join(sorted(solution)))

        return solution_sets
