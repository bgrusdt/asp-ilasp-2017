from asp_clingo.ast import parse_program
from asp_clingo.program import Program
from utils_for_tests import as_str_set, expect_failure

TEST_NAME = "blabla"
TEST_NAME2 = "bloblup"
assert TEST_NAME != TEST_NAME2


def test_program_set_name():
    prog = Program(TEST_NAME)
    assert prog.name == TEST_NAME
    assert prog.rules()[0].name == TEST_NAME

    prog.name = TEST_NAME2
    assert prog.name == TEST_NAME2
    assert prog.rules()[0].name == TEST_NAME2


def test_program_extend():
    prog = Program(TEST_NAME)
    prog.extend(parse_program("a.b. c :- a.", rename_base=TEST_NAME))
    assert prog.name == TEST_NAME
    assert as_str_set(prog.rules()) == {"#program {}.".format(TEST_NAME),"a.", "b.", "c :- a."}

def test_program_free_rules():
    prog = Program(TEST_NAME)
    assert prog.free_rules() == []
    prog.extend(parse_program("a.b.c.", rename_base=TEST_NAME))
    assert as_str_set(prog.free_rules()) == {"a.", "b.", "c."}

def test_program_extend_with_other_program_name():
    prog = Program(TEST_NAME)
    expect_failure(
        lambda: prog.extend(parse_program("a.b. c :- a.", rename_base=TEST_NAME2)),
        expected=ValueError
    )


def test_program_multiple_names_legal():
    prog = Program(TEST_NAME)
    prog_ast = parse_program("a :- b.", rename_base=TEST_NAME) + parse_program("b.", rename_base=TEST_NAME)
    prog.extend(prog_ast)
    assert as_str_set(prog.rules()) == {"#program {}.".format(TEST_NAME), "a :- b.", "b."}


def test_program_multiple_names_illegal():
    prog = Program(TEST_NAME)
    prog_ast = parse_program("a :- b.", rename_base=TEST_NAME) + parse_program("b.", rename_base=TEST_NAME2)
    expect_failure(
        lambda: prog.extend(prog_ast),
        expected=ValueError
    )


def test_from_str():
    prog = Program.from_str(TEST_NAME,"""
        a :- b, c.
        d.
    """)

    assert prog.name == TEST_NAME
    assert prog.rules()[0].name == TEST_NAME
    assert as_str_set(prog.rules())
