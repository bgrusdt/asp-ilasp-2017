import clingo
from utils import do_if
from asp_clingo.ast import *


from utils_for_tests import as_str_set, expect_failure


def test_ASTVariable():
    v = ASTVariable("test123")
    assert v.name == "test123"


def test_ASTSymbol_from_int():
    v = ASTSymbol(12)
    assert str(v) == "12"


def test_ASTSymbol_from_string():
    v = ASTSymbol("bla")
    assert v.type == clingo.ast.ASTType.Symbol
    assert str(v) == "bla"


def test_ASTSymbol_from_symbol():
    v = ASTSymbol(clingo.parse_term("a3"))
    assert str(v) == "a3"


def test_ASTSymbol_fail():
    expect_failure(
        lambda:ASTSymbol(12.4),
        ValueError
    )

def test_get_sign__none():
    assert get_sign(negate=False) is clingo.ast.Sign.__dict__["None"]


def test_get_sign__negated():
    assert get_sign(negate=True) is clingo.ast.Sign.Negation


def test_ASTLiteral():
    atom = object()
    v = ASTLiteral(atom, negate_sign=True)
    assert v.atom == atom
    assert v.sign == clingo.ast.Sign.Negation


def test_ASTFunction():
    name = "abc"
    arguments = [object()]
    external = object()
    v = ASTFunction(name, arguments, external)
    assert v.name == name
    assert v.arguments == arguments
    assert v.external == external


def test_ASTFunction__default():
    v = ASTFunction("abc", [])
    assert not v.external


def test_ProgramMarker():
    marker = ASTProgramMarker("mark123")
    assert str(marker) == "#program mark123."
    assert marker.parameters == []


def test_create_rule():
    head = object()
    body = object()
    ast = ASTRule(head=head, body=body)
    assert ast.head == head
    assert ast.body == body


def test_ASTConstraintHead():
    head = ASTConstraintHead()
    assert head.atom.type == clingo.ast.ASTType.BooleanConstant
    assert not head.atom.value


def test_create_literal__simple():
    literal = create_literal("tom")
    assert literal.type is clingo.ast.ASTType.Literal
    assert str(literal) == "tom"


def test_create_literal__args():
    literal = create_literal("tom", [1, "test"])
    assert literal.type is clingo.ast.ASTType.Literal
    assert str(literal) == "tom(1,test)"


def test_create_literal__negated():
    literal = create_literal("flies",["bird"], negate=True)
    assert literal.type is clingo.ast.ASTType.Literal
    assert str(literal) == "not flies(bird)"


def test_parse_program():
    prog = parse_program("a. b :- e.", rename_base="liz")
    assert len(prog) == 3
    assert prog[0].name == "liz"
    assert prog[0].type is clingo.ast.ASTType.Program
    assert prog[1].type is clingo.ast.ASTType.Rule
    assert prog[2].type is clingo.ast.ASTType.Rule


def test_walk_ast():
    program = parse_program("e(b(3,2)). { a; b} :- e(X), X > 2.")
    found = []
    walk_ast(program, do_if(is_symbolic_atom, found.append))
    assert as_str_set(found) == { "e(b(3,2))", "a", "b", "e(X)"}

def test_find_symbolic_atom_roots_aggregates():
    program = parse_program(":- 3 < #count { a(R) : b(R) }.")
    found = []
    walk_ast(program, do_if(lambda x: is_symbolic_atom(x) or is_function(x), found.append))
    assert { "a(R)", "b(R)" } == as_str_set(found)

# def test_find_program_names_1():
#     name = list(find_program_names(parse_program("a. b.", rename_base="blob")))
#     assert name == ["blob"]
#
#
# def test_find_program_names_2():
#     names = list(find_program_names(parse_program("a. b. #program qq. d.")))
#     assert len(names) == 2
#     assert set(names) == {"base", "qq"}


def test_atom_as_literal():
    atom = clingo.Function("e(a)")
    literal = atom_as_literal(atom)
    assert literal.type == clingo.ast.ASTType.Literal
    assert str(literal) == "e(a)"
    assert literal.atom.term.symbol.type is atom.type
    assert type(literal.atom.term.symbol) is clingo.Symbol


def test_is_program_marker():
    assert is_program_maker(ASTProgramMarker("test"))
    assert not is_program_maker(parse_program("a.")[1])


def test_create_constraint():
    atoms = (clingo.Function(name) for name in ["a(e1)","a(e2)","a(e19)"])
    constraint = create_constraint(atoms)
    assert isinstance(constraint, clingo.ast.AST)
    assert str(constraint) == "#false :- a(e1); a(e2); a(e19)."

def test_create_constraint_empty():
    #TODO is this the expected bahaviour?? yes
    # or would `#false :- #false.` be better??
    constraint = create_constraint([])
    assert str(constraint) == "#false."


def test_parse_fails():
    expect_failure(
        #uh, this will print to stderr :=( ...
        lambda: parse_program("BLABLABLA"),
        RuntimeError
    )
