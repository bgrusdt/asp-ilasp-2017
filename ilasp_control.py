import time

import clingo

from utils.profile import profile, profile_section, profile_iter_next, get_active_profiling_section
from utils.logger import IlaspLogger
from utils.ilasp_utils import extract_active_rule_ids

from six.moves import filter

from constants import (
    VIOLATING_LOGGER_NAME
)

LOGGER = IlaspLogger.getLogger(__name__)


# do NOT EVER inherit from `clingo.Control` DO NOT DO THAT DON'T!
# (it will cause seemingly randoms segmentation faults, possible at
#  places you don't expect them to occur at all, like in tests
#  which don't use any clingo methods... )
class Control(object):

    def __init__(self, *args, **kwargs):
        wrap_clingo = kwargs.get("wrap_clingo_instance", None)
        if wrap_clingo is None:
            wrap_clingo = clingo.Control(*args, **kwargs)
        # we disabled setters so uh, don't write self.xxxx = xxx even in the init method
        self.__dict__['_clingo'] = wrap_clingo
        self.__dict__['violating_logger'] = IlaspLogger.getLogger(VIOLATING_LOGGER_NAME)

    def __getattr__(self, item):
        # gets called if a field/method _is not found_ => forward to clingo
        # as getattr is only called if it is not found we call __getattribute__ on
        return self._clingo.__getattribute__(item)

    def __setattr__(self, key, value):
        raise NotImplementedError("forwarding setter to wrapped Controll is not supported for now")

    def add_many(self, subprog, vars, rules):
        """
        Adds multiple rules with the same set of variables to
        this Control instance

        :param List[str] rules:
        :param str for_subprog:
        :return:
        """
        for rule in rules:
            self.add(subprog, vars, rule_as_str(rule))

    @profile(name="Control.run_negative_phase")
    def run_negative_phase(self, context, n):
        """
        runs the negative solve phase adding all found constraints to this Controll
        instance to a constraints "program"

        while doing the the external `ex(neg)` is set to True

        :param int n:
        :return None: found constraints are added to clingo.Controll to the constraints programm
        """
        LOGGER.info('start negative phase for n=%s...', n)

        violating_solution_rids = set()
        self.assign_external(clingo.Function("ex", [clingo.Function("neg")]), True)

        solving_start = time.time()
        LOGGER.info('start solving negative phase ...')

        with self.solve_iter() as solutions:
            solutions = profile_iter_next(solutions, "Control.run_negative_phase.next_solution")
            for idx_model, model in enumerate(solutions):
                rids = extract_active_rule_ids(model, LOGGER)
                if not rids:
                    LOGGER.warn('found empty violating solution, this should not be possible (model str: %s)',
                                str(model))
                    continue

                if rids in violating_solution_rids:
                    self.violating_logger.warn("model %s is a duplicate model: %s", idx_model, ', '.join(rids))
                    continue
                else:
                    violating_solution_rids.add(rids)

                self.violating_logger.info('violating rules for model %s: %s', idx_model, ', '.join(rids))

        constraints = [
            ':-' + ', '.join('active(' + item + ')' for item in rids) + '.'
            for rids in violating_solution_rids
        ]

        if constraints:
            # LOGGER.info('solve again with last constraint added ...for length n=%s and iteration %s...', n, iternb)
            concatenated = ' '.join(constraints)
            self.add("constraints", [], concatenated)
            LOGGER.debug('#program constraints.' + concatenated)
            with profile_section('Control.run_negative_phase.ground_constraints'):
                self.ground([("constraints", [])], context)
        else:
            LOGGER.debug('no constraints added.')

        solving_duration = time.time() - solving_start
        LOGGER.info('solving neg phase for length %s took %s seconds.', n, solving_duration)

    @profile(name="Control.run_positive_phase")
    def run_positive_phase(self,  n, validate_fn):
        """
        runs the positive solve phase and returns all solutions

        while doing so it assigns the external `ex(neg)` the value false

        :param clingo.Control metaProg
        :param int n:
        :param validate_fn: a function called on each solution, deciding if it should be discarded
        :return: a set of answersets containing the ruleids of a found solution
        """
        results = set()

        self.assign_external(clingo.Function("ex", [clingo.Function("neg")]), False)

        LOGGER.info("start solving positive phase...")

        with self.solve_iter() as solutions:
            with_rids_extracted = (extract_active_rule_ids(model, LOGGER) for model in solutions)
            with_validation = ((rids, validate_fn(rids)) for rids in with_rids_extracted)
            with_profiling = profile_iter_next(with_validation, "Control.run_positive_phase.next_solution")
            # we need to filter out bad solutions AFTER applying next step profiling
            without_bad_solutions = (rids for rids, is_valid in with_profiling if is_valid)
            with_enumeration = enumerate(without_bad_solutions)
            for nr, rids in with_enumeration:
                LOGGER.info('positive phase for length %s solution nr. %s', n, nr)
                results.add(rids)

        if not results:
            LOGGER.info('no solution found for length %s', n)

        return results


def rule_as_str(rule):
    if isinstance(rule, str) or isinstance(rule, unicode):
        return rule
    else:
        if rule.meta_str:
            return rule.meta_str
        else:
            return rule.str
