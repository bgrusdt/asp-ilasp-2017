import json
import os.path
import ConfigParser

from utils.logger import IlaspLogger
from utils.ilasp_utils import TypeManager
from asp_representation.background import Background

LOGGER = IlaspLogger.getLogger(__name__)


class ProblemInstance(object):

    # FIXME more missing and turn all *_path into value
    __slots__ = [
        'examples',
        'background',
        'mode_declaration_path',
        'type_manager'
    ]

    def __init__(self, examples, background, mode_declaration_path, type_manager):
        self.examples = examples
        self.background = background
        self.mode_declaration_path = mode_declaration_path
        self.type_manager = type_manager

    @classmethod
    def from_config(cls, path):
        base_dir = os.path.dirname(path)
        config = ConfigParser.SafeConfigParser()
        config.read(path)

        background_path = config.get('ProblemInstance', 'background')
        background_path = os.path.join(base_dir, background_path)
        LOGGER.info('background knowledge file: %s', background_path)

        example_path = config.get('ProblemInstance', 'examples')
        example_path = os.path.join(base_dir, example_path)
        LOGGER.info('example file: %s', example_path)

        mode_declaration_path = config.get('ProblemInstance', 'modes')
        mode_declaration_path = os.path.join(base_dir, mode_declaration_path)
        LOGGER.info('mode declarations file: %s', mode_declaration_path)

        type_manager = TypeManager.from_string(config.get("TypeValues", "consts"))

        problem = cls.from_paths(
            example_path=example_path,
            background_path=background_path,
            mode_declaration_path=mode_declaration_path,
            type_manager=type_manager
        )
        return problem, config

    @classmethod
    def from_paths(cls, example_path, background_path, mode_declaration_path, type_manager):
        examples = Examples.from_path(example_path)
        background = Background.from_path(background_path)
        #FIMXE load background
        #FIXME load mode_declarations
        return cls(
            examples=examples,
            background=background,
            mode_declaration_path=mode_declaration_path,
            type_manager=type_manager
        )


class Examples(object):
    __slots__ = ['pos', 'neg']

    def __init__(self, pos, neg):
        self.pos = pos
        self.neg = neg

    @classmethod
    def from_path(cls, path):
        try:
            with open(path) as json_data:
                examples_dict = json.load(json_data)
            return cls(examples_dict["pos"], examples_dict["neg"])
        except ValueError as e:
            message = \
                ("The examples json file ({}) must contain one dictionary entry \"pos\" and one \"neg\" " +
                "for the declaration ofpositive and negative examples respectively: {}").format(path, e)
            raise ValueError(message)