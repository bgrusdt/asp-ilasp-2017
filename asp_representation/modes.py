from utils.generators import IDGenerator
from utils.rules_utils import PredicateAccessor
import logging
import re
import utils

TRICKS = ['antireflexive', 'symmetric', 'positive']
logger = logging.getLogger(__name__)


class ModeDeclaration:

    pattern_marker = '$'
    const_pattern = pattern_marker + 'c' + pattern_marker
    var_pattern = pattern_marker + 'V' + pattern_marker
    id_generator = IDGenerator('m')

    def __init__(self, mode_declaration, mode_type):
        """
        :param dict mode_declaration: contains mode declaration string, vartypes and constypes
        :param str mode_type: one of: modeb, modeha or modeh
        """
        original_content = str(mode_declaration['content'].strip())
        # just exclude mode() around
        if mode_type == 'M_b':
            self._mode_type = 'body'
        elif mode_type == 'M_ha':
            self._mode_type = 'head_aggregate'
        elif mode_type == 'M_h':
            self._mode_type = 'head'
        else:
            self._mode_type = None
            logger.warn('Mode without type initiated.')

        self._mode_id = ModeDeclaration.id_generator.nextId()
        self._const_patterns = None
        pred_accessor = PredicateAccessor([original_content.replace("$", "")])

        self._tricks = {}
        for trick in TRICKS:
            self._tricks[trick] = False

        # check arguments
        # always only one predicate, namely modeb/ha/h !!
        args = pred_accessor.extract_clingo_arguments()[0]
        variables = pred_accessor.extract_variables()
        if variables:
            variables = variables[0]

        nbVars = len(variables)
        self._nb_vars = nbVars
        self.variables = variables

        # 1.check if final extra argument
        last_argument = args[-1]
        extraArg_exists = False
        if str(last_argument.type) == 'Function':
            last_argument_args = last_argument.arguments
            for arg in last_argument_args:
                arg_str = str(arg)
                if arg_str in TRICKS:
                    self._tricks[arg_str] = True
                    extraArg_exists = True
                else:
                    extraArg_exists = False

        if extraArg_exists:
            end_args = -1
        else:
            end_args = len(args)

        if self._mode_type == 'body':
            start_args = 1
            self._nb_occs_per_rule = int(str(args[0]))
        else:
            start_args = 0

        content = args[start_args: end_args][0]  # is always only one predicate
        self.ast_content = content

        # content as string not ast with const and var patterns
        ast_type = str(content.type)
        if ast_type == "Function":
            name = content.name
        elif ast_type == 'Symbol':
            name = str(content)
        else:
            logger.warn('ModeDeclaration with type %s.', ast_type)
            raise ValueError('ModeDeclaration with type ' + ast_type)

        self._name = name
        content = "".join(str(content).split())
        str_mode_content = "".join(str(original_content).split())

        regex = r"(?<!mode)" + name
        match = re.search(regex, str_mode_content)
        content_start = match.start()
        content_end = content_start + len(content) + str_mode_content.count(ModeDeclaration.pattern_marker)

        self._original_content = str_mode_content[content_start: content_end]

        # constants and variables
        self._all_var_types = map(str, mode_declaration['vartypes'])
        self._all_const_types = map(str, mode_declaration['consttypes'])
        self._nbConsts = len(self._all_const_types)

    def get_ctypes(self):
        return list(self._all_const_types)

    def get_mode_id(self):
        return self._mode_id

    def get_mode_type(self):
        return self._mode_type

    def get_name(self):
        return self._name

    def get_nb_consts(self):
        return self._nbConsts

    def get_nb_vars(self):
        return self._nb_vars

    def get_original_content(self):
        return self._original_content

    def get_vtype_dict(self):
        return list(self._all_var_types)

    def is_aggregate(self):
        return self._mode_type == 'head_aggregate'

    def is_anti_reflexive(self):
        return self._tricks["antireflexive"]

    def is_head(self):
        is_aggregate = self.is_aggregate()
        return is_aggregate or self._mode_type == 'head'

    def is_positive(self):
        return self._tricks["positive"]

    def is_symmetric(self):
        return self._tricks["symmetric"]

    def get_const_patterns(self, type_manager):
        # if self._mode_type == 'head_aggregate':
        if self._const_patterns is None:
            self._set_const_patterns_head_agg(type_manager)
        # TODO: not a deep copy, is a list of lists?!
        return list(self._const_patterns)

    @staticmethod
    def replace_constants_in_heads(head_strings, candidate_head):
        """
        :param list of str head_strings
        :param CandidateRulePart candidate_head
        :return list of lists of str
        """
        const_combis = candidate_head.get_constant_combis()
        if not const_combis:
            return [head_strings]
        dup_indices = candidate_head.get_duplicate_indices()
        forbidden_combis = []
        for dup_idxs in dup_indices:
            list_consts = []
            for ccombi in const_combis:
                const_indices = candidate_head.get_const_indices(dup_idxs)
                consts = [tuple(utils.get_list_elems_by_tuple(ccombi, const_indices[idx])) for idx in range(len(const_indices))]
                consts = set(consts)
                if consts in list_consts:
                    forbidden_combis.append(ccombi)
                else:
                    list_consts.append(consts)

        all_replacements = []
        for ccombi in const_combis:
            if ccombi not in forbidden_combis:
                replacement = utils.ilasp_utils.replace_pattern_in_predicates(head_strings[:], ccombi,
                                                                              ModeDeclaration.const_pattern)
                all_replacements.append(replacement)
        return all_replacements

    def _build_dicts(self, uniqueTypes, all_types):

        type_indices_dict = {}
        # dictionary entry for each unique type
        for t in uniqueTypes:
            type_indices_dict.update({t: []})

        # save indices which belong to which type
        for idx_elem, t in enumerate(all_types):
            type_indices_dict[t].append(idx_elem)

        return type_indices_dict

    def get_nb_occs_per_rule(self):
        """
        returns the number of times this mode object can be applied in one rule
        this only holds for body modes, 0 is returned if this mode is a head mode
        """
        if(self._mode_type == 'body'):
            return self._nb_occs_per_rule
        else:
            return 0

    def get_vtypes(self):
        return list(self._all_var_types)

    @staticmethod
    def getMaxNbBodyElems(mode_list):
        """
        :param dict modeList: values are ModeDeclaration objects
        """
        nb = 0
        for mode in mode_list.values():
            nb += mode.get_nb_occs_per_rule()

        return nb

    def _set_const_patterns_head_agg(self, type_manager):
        """if head is aggregate, constants must be replaced directly; generates all possible constant replacements.
        :param TypeManager type_manager
        """
        type_val_dict = type_manager.get_ctype_val_dict()
        ctypes = self.get_ctypes()
        vals = []
        for const_type in ctypes:
            vals += list(type_val_dict[const_type])

        self._const_patterns = map(str, vals)

    def __eq__(self, other):
        if self.__class__ != other.__class__:
            return False

        return self.__dict__ == other.__dict__
