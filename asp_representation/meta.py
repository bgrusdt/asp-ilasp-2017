from utils import flattened_iter
from utils.logger import IlaspLogger
from utils.ilasp_utils import StrRule

from constants import (
    SUBPRG_BACKGROUND_KNOWLEDGE,
    SUBPRG_POSITIVE_EXAMPLES,
    SUBPRG_NEGATIVE_EXAMPLES,
    SUBPRG_CHOOSE_ACTIVES,
    SUBPRG_STEP,
    SUBPRG_EXNEG,
    SUBPRG_META
)


LOGGER = IlaspLogger.getLogger(__name__)


class MetaRules(object):
    __slots__ = ['data']

    def __init__(self, rules):
        self.data = rules

    def __eq__(self, other):
        return self.data == other.data

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, repr(self.data))

    def prog_parts_iter(self):
        """
        returns a iterator yielding `(prog_name, prg_vars, prg_content_string)` tuples,
        which represents all rules in this object

        :return:
        """
        return map(
            lambda rule: (rule.prg_name, rule.prg_vars, rule.prg_content_string),
            flattened_iter(self.data)
        )


def constructMetaRules(problem, id_gen):
    data = []
    # background
    meta_background = constructMetaBackground(problem.background, problem.type_manager)
    data.append(meta_background)

    # FIXME don't load data here
    meta_examples = constructMetaExamples(problem.examples, id_gen)
    data.append(meta_examples)

    # search space
    meta_aux_choice = construct_meta_aux_choiceRule()
    data.append(meta_aux_choice)

    meta_step = constructStep()
    data.append(meta_step)

    meta_exneg = constructIlaspExNeg()
    data.append(meta_exneg)

    data.append([StrRule(prg_name=SUBPRG_META, prg_vars=[], prg_content_string="#project active/1.")])
    LOGGER.debug("#program " + SUBPRG_META + ".")
    LOGGER.debug("#project active/1.")
    return MetaRules(data)


def constructMetaExamples(examples, id_gen):
    """
    Adds Meta rules for examples to logic program
    :param example_pos: list of strings containing predicates like "value((1,1),1)" (without dot in the end)
    representing positive examples
    :param example_neg: -"- representing negative examples
    """
    all_rules = []
    # Positive examples
    name = SUBPRG_POSITIVE_EXAMPLES
    if(examples.pos):
        LOGGER.debug('#program ' + name + '.')

    for ex_pos in examples.pos:
        example_id = id_gen.nextId()
        pos_rules = []

        ex_id = 'ex(' + example_id + ').'
        not_covered_constraint = ':- not covered(' + example_id + ').'

        all_rules.append(StrRule(prg_name=name, prg_vars=[], prg_content_string=ex_id))
        all_rules.append(StrRule(prg_name=name, prg_vars=[], prg_content_string=not_covered_constraint))

        LOGGER.debug(ex_id)
        LOGGER.debug(not_covered_constraint)

        for incl in ex_pos['inclusions']:
            pos_rules.append(' e(' + incl + ',' + example_id + ')')

        for excl in ex_pos['exclusions']:
            pos_rules.append(' not e(' + excl + ',' + example_id + ')')

        str_pos_rules = 'covered(' + example_id + ') :- ' + ','.join(pos_rules) + '.'

        all_rules.append(StrRule(prg_name=name, prg_vars=[], prg_content_string=str_pos_rules))
        LOGGER.debug(str_pos_rules)

    # Negative examples
    name = SUBPRG_NEGATIVE_EXAMPLES
    if(examples.neg):
        LOGGER.debug('#program %s .', name)

    for ex_neg in examples.neg:
        neg_rules = []

        for incl in ex_neg['inclusions']:
            neg_rules.append(' e(' + incl + ',neg)')

        for excl in ex_neg['exclusions']:
            neg_rules.append(' not e(' + excl + ',neg)')

        str_neg_rules = 'violating :- ' + ','.join(neg_rules) + '.'

        all_rules.append(StrRule(prg_name=name, prg_vars=[], prg_content_string=str_neg_rules))
        LOGGER.debug(str_neg_rules)

    return all_rules


def construct_meta_aux_choiceRule():
    """
    META(Aux,n) and lengths rules are generated.

    :return List[StrRule]: list of rules
    """
    all_rules = []
    name = SUBPRG_CHOOSE_ACTIVES
    # FIXME we create a new Control every time so:
    # 1. having the `step(n)` part in the restriction is now unnecessary
    # 2. having `@getNewActives(n)` is no longer necessary
    # benefit in having it
    all_chooses = [
        "{active(X)} :- X=@getRuleIds().",
        ":- not #sum {X,R : active(R), length(R,X)} == n , step(n).",
        ":-  ex(neg), not violating."
    ]

    LOGGER.debug('#program ' + name + ' (n).')
    for choose_rule in all_chooses:
        all_rules.append(StrRule(prg_name=name, prg_vars=["n"], prg_content_string=choose_rule))
        LOGGER.debug(choose_rule)

    return all_rules


def constructStep():
    name = SUBPRG_STEP
    rule = [StrRule(prg_name=name, prg_vars=["t"], prg_content_string="step(t).")]
    LOGGER.debug("#program " + name + " (t).")
    LOGGER.debug("step(t).")

    return rule


def constructIlaspExNeg():
    name = SUBPRG_EXNEG
    # external because we have the same control for positive and negative phase
    rules = [StrRule(prg_name=name, prg_vars=["t"], prg_content_string="#external ex(t).")]
    LOGGER.debug("#program " + name + " (t).")
    LOGGER.debug("#external ex(t).")

    return rules


def constructMetaBackground(background, typeManager):
    name = SUBPRG_BACKGROUND_KNOWLEDGE
    all_rules = []
    meta_background = background.as_rules()
    all_rules.extend(meta_background)

    # constant types
    type_dict = typeManager.get_ctype_val_dict()
    names = type_dict.keys()
    LOGGER.debug('#program ' + name + '.')
    for ctype in names:
        values = type_dict[ctype]
        for val in values:
            prog_content = ctype + '(' + str(val) + ').'
            all_rules.append(StrRule(prg_name=name, prg_vars=[], prg_content_string=prog_content))
            LOGGER.debug(prog_content)

    return all_rules
