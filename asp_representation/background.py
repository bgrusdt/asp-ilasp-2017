import clingo
from asp_clingo.ast import (
    walk_ast,
    ASTLiteral, ASTFunction,
    ASTVariable,
    is_symbolic_atom,
    is_function as is_ast_function,
    parse_program
)
from constants import (
    ASP_NAME_OF_E,
    ASP_NAME_OF_EX_ATOM,
    ASP_NAME_OF_EX_VAR,
    SUBPRG_BACKGROUND_KNOWLEDGE
)

from utils import do_if

from utils.ilasp_utils import StrRule

from utils.logger import IlaspLogger

LOGGER = IlaspLogger.getLogger(__name__)


class Background(object):
    __slots__ = ['_program', '_source']

    @classmethod
    def from_path(cls, path_to_bg_data):
        with open(path_to_bg_data) as background_file:
            as_str = background_file.read()
        return cls.from_str(as_str)

    @classmethod
    def from_str(cls, str):
        #FIXME make this a constante
        prog = parse_program(str, rename_base="background")
        rewrite_program(prog)
        return cls(prog, source=str)

    def __init__(self, prog, source=None):
        self._program = prog
        self._source = source

    def __eq__(self, other):
        return self._program == other._program

    def __str__(self):
        return "{}({})".format(self.__class__.__name__, repr(self._program))

    def as_rules(self):
        # skip the` #program background` part
        return [
            StrRule(prg_name=SUBPRG_BACKGROUND_KNOWLEDGE, prg_vars=[], prg_content_string=repr(rule))
            for rule in self._program[1:] ]

    def as_string_rules(self):
        return [repr(rule) for rule in self._program]


def rewrite_program(program_ast):
    """rewrites the program ast IN PLACE!

    :param program_ast:
    :return:
    """
    # 1. wrap atoms

    walk_ast(program_ast, gate_ast_node_in_place)

    # 2. add ex(__EX__) to all rules
    for rule in program_ast:
        if rule.type is clingo.ast.ASTType.Rule:
            rule.body.append(ASTLiteral(
                atom=clingo.ast.SymbolicAtom(
                    term=ASTFunction(
                        name=ASP_NAME_OF_EX_ATOM,
                        arguments=[ASTVariable(ASP_NAME_OF_EX_VAR)]
                    )
                )
            ))


def gate_ast_node_in_place(ast_node):
    """
    we need a special function different to gate_atom_in_place,
    as `a(R)` in `#count { a(R) : b(R) }` is for some reason a
    function, not a symbolic atom
    :param ast_node:
    :return:
    """
    if is_symbolic_atom(ast_node):
        gate_atom_in_place(ast_node)
        return True
    elif is_ast_function(ast_node):
        gate_function_in_place(ast_node)
        return True
    return False


def gate_function_in_place(ast_node):
    old_name = ast_node.name
    old_args = ast_node.arguments
    new_old = ASTFunction(
        name=old_name,
        arguments=old_args
    )
    ast_node.name = ASP_NAME_OF_E
    ast_node.arguments = [
        new_old,
        ASTVariable(ASP_NAME_OF_EX_VAR)
    ]


def gate_atom_in_place(symbol_ast):
    old_term = symbol_ast.term
    symbol_ast.term = ASTGatedAtom(
        atom=old_term,
        ex_id=ASTVariable(ASP_NAME_OF_EX_VAR)
    )


def ASTGatedAtom(atom, ex_id):
    return ASTFunction(
        name=ASP_NAME_OF_E,
        arguments=[
            atom,
            ex_id
        ]
    )


# def parse_wrapped_atom(atom, rule_id, negate=False):
#     """
#
#     ```
#     assert parse_wrapped_atom("a1", "ex1", negate=True) == "not e(a1,ex1)"
#     ```
#
#     :param atom:
#     :param rule_id:
#     :param negate:
#     :return:
#     """
#     atom_symbol = clingo.parse_term(atom)
#     atom = clingo.ast.SymbolicAtom(
#         term=ASTGatedAtom(
#             atom=ASTSymbol(atom_symbol),
#             ex_id=ASTSymbol(rule_id)
#         )
#     )
#     return ASTLiteral(
#         atom=atom,
#         negate_sign=negate
#     )

#
# def create_cost_rule(rid, cost):
#     return ASTRule(
#         head=ASTLiteral(
#             atom=clingo.ast.SymbolicAtom(
#                 term=ASTFunction(
#                     name=ASP_NAME_OF_COST_PREDICATE,
#                     arguments=[
#                         ASTSymbol(rid),
#                         ASTSymbol(cost)
#                     ]
#                 )
#             )
#         ),
#         body=[]
#     )
#
#
