
# ILASP (2017)


## Checks for excluding rules

### Checks concerning the variable arrangements

**checkRuleInternal**:

makes checks on predicates that appear more than once in this body:

- They must not be equal wrt all their variables, e.g. a(V0,V1), a(V0,V1).
- They must not have completely different variables that do not appear elsewhere in the body, e.g. a(V0,V1), a(V2,V3), c(V4).
- checks for all variables of all duplicate predicates if they appear always at the same slot and only in the duplicate
predicates and nowhere else, e.g. a(V0,V1), a(V0,V2) is discarded, but a(V0,V1), a(V2,V0) is kept.
- further checks on duplicate predicates where one predicate has only one unique variable at least twice,
e.g. a(V0,V0,V0), a(V0,V1,V2), c(V3). Checks if none of the variables of the duplicate predicates (except that unique
variable), appears anywhere else in the body. If so, the rule is discarded. Since if a(V0,V1) is True, a(V0,V0) will always
be True as well and to keep the exact same meaning, V1 must not appear anywhere else in the body.

#### generated variable arrangements

For each number of variables appearing in one rule, one *CombiTree* is cretaed. It lists all possible variable arrangements, e.g. for two variables, it will generate *[['V0', 'V1'], ['V0', 'V0']]*. *[V1,V0]* is not generated, since it is semantically equal to the generated combination *[V0,V1]*. The generated arrangements are already abstracted, i.e. in the given example, it will never create *[V1,V0]* instead of *[V0,V1]*.

**postprocess_var_arrangements**

For each unique possible combination of body predicates, the generated variable arrangements are postprocessed. If there are duplicate body predicates, some arrangements become identical, e.g. the variable arrangement *[V0,V1,V2,V0]* is equal to the arrangement *[V0,V1,V1,V2]* if the first two and the last two variable refer to the same predicate respectively: *a(V0,V1), a(V2, V0)* is identical to *a(V2, V0), a(V0,V1)* since it does not matter in which order the predicates appear. Since the arrangements are abstracted, *a(V2, V0), a(V0,V1)* must be converted to *a(V0, V1), a(V1,V2)*.


### implemented tricks from ILASP-Paper

1. Types
   The input json-file, describing the mode declarations of the problem to solve, contains for each variable/constant a type. One variable/constant
   can have only one type. This type is respected in all rules.  *:- a(V0), b(V0), c(V1).* can not be included as rule if the
   variable in the body predicate 'a' has type 'type_a' and the variable in the body predicate 'b' has type 'type_b'.

2. Anti-Reflexives
   Any 2-ary predicate (where both arguments are variables) specified in the input json-file describing the mode declarations of the
   problem to solve, can be declared as *anti-reflexive*. This means that such a predicate is not included in a rule with twice
   the same variable, e.g. *:- a(X,X)* if predicate 'a' is *anti-reflexive*.

3. Symmetric
   Any 2-ary predicate (where both arguments are variables) specified in the input json-file describing the mode declarations of the
   problem to sove, can be declared as *symmetric*. This means that *a(V,W)* is semantically the same as *a(W,V)*. Therefore the following rules would
   not be included in the searchspace provided that the predicate 'a' is *symmetric*:
       - *:- a(V,W), a(W,V).*
    
       - *:-a(V,W), b(V), c(W).* together with *:- a(W,V), b(V), c(W).*

4. Positive
  Any body predicate can be declared as being *positive*, i.e. it will then not appear negated in any rule.

5. Constants
  All constants are defined in the file **ilasp.properties** in the project folder under the section **[Meta]**.
      1. *max_rule_length*: (int) The maximal length a rule can have
      2. *max_hypothesis_cost*: (int) The maximal cost of an hypothesis
      3. *maxv*: (int) The maximal number of different variables that can appear in one rule. If set to 3 for instance, *:- a(V0), b(V1), c(V2), d(V3).* would not be included in the searchspace, but *:- a(V0), b(V0), c(V0), d(V0).* would be included.
      4. *minh* and *maxh*: (int) The minimal/maximal number of predicates that can appear in a head aggregate.
      5. *disallow_multiple_head_vars*: (bool) if set to 1, each head can only have one unique variable, *1{value(V,1); value(V,2)}1* would be allowed for instance, but *1{value(V0,1); value(V1,2)}1* would not be allowed.
      6. *only_constraints*: This was rather implemented for debugging purposes. If set to 1, no other rules than constraints are included in the searchspace.




### Head aggregates
**TO BE IMPLEMENTED**
If a predicate appears in the body, none of the predicates in the head aggregate can be equal to that body predicate, e.g. *{a(V)} :- a(V), b(X).* should not be included, however *{a(V)} :- a(X),b(V).* should be included in the searchspace.

### Single Heads
**TO BE IMPLEMENTED**
no checks yet.


## INPUT FILES
### Mode Declarations File (json)
Object with keys **M_h**, **M_ha**, **M_b** describing the head, head aggregate and body predicates respectively.
The values for these contain objects (or they are just empty) with the following structures:

- **content**: mapping to a string of form *modeb(n,pred($V$,$c$),(antireflexive,symmetric))* where n is an integer value specifying the maximal
  number of times that this body predicate may be repeated in a rule's body; all variables are denoted by $V$  and constants by
  $c$. The third argument is optional, it can comprise one or more of: *antireflexive,symmetric,positive*.
  In the case of head or head aggregate predicates, 
  content maps to a string of form *a(value($V$,$c$,..))* for a being either *modeh* or *modeha*;
- **vartypes**: mapping to a list of strings, specifying, in order of appearance in the predicate, the types of the variables
- **consttypes**: as vartypes, but referring to the types of the constants. 
	
### Examples File (json)
Object with keys **pos**, **neg**.
Both keys map to an array of objects each of which has the two keys **inclusions** and **exclusions**. These two keys in turn map
to an array of predicates (strings), e.g. 	{"inclusions": ["value((1,1),1)", "value((1,3),1)"], "exclusions": ""} .

### Background File	(.lp)
File containing ASP background rules. Comments with %. Can contain empty lines.

## PROPERTIES FILE
Is located in the main project directory.

* **required sections**:

	**[ProblemInstance]**. In this section, the keys **background**, **examples** and **modes** must point to the location of the respective input files.

	**[TypeValues]**. Key: consts; value: dictionary from constant type to list of possible values, e.g.

	*[TypeValues]*

	*consts={"number": [1,2,3,4]}*




* **optional sections**:

	**[Costs]**: specifying all costs for which the searchspace shall be generated; only applies if only the searchspace is build without running ilasp.

	**[Ignored_sol_preds]**: values are predicates that shall be ignored, when logging the solution

	**[Meta]**: contains one or more of the constants specified under 4. in *implemented tricks from Ilasp paper* above.


## LOGGING
Logging is configured in the file *logging-config.conf* inside the *resources*-directory.

## RUNNING ILASP
1. Run without arguments: ilasp runs normally
2. Run with argument '0': only the searchspace is build and logged

